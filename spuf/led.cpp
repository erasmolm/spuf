//
// Created by Erasmo La Montagna on 09/06/2020.
//

#include "led.h"

LED::LED() : SPuf()
{
	// KEY 64 as default case
	steps = led_steps[LED_KEY_64];
	LED::keySize = led_key_sizes[LED_KEY_64];
}

LED::LED(led_key_size_t mode) : SPuf()
{
	led_key_size_t key_setting = LED_KEY_64;
	
	switch (blockMode)
	{
	case LED_BM_ECB:
	case LED_BM_CBC:
		{
			//KEY must be 128
			key_setting = LED_KEY_128;
			break;
		}
	case LED_BM_SPLIT:
		{
			//KEY must be 64
			key_setting = LED_KEY_64;
			break;
		}
	default:
		break;
	}

	steps = led_steps[key_setting];
	LED::keySize = led_key_sizes[key_setting];
}

LED::LED(led_block_mode_t mode, const std::array<uint8_t, 16> keyConfig) : SPuf(keyConfig)
{
	blockMode = mode;
	led_key_size_t key_setting = LED_KEY_64;
	
	switch (blockMode)
	{
	case LED_BM_ECB:
	case LED_BM_CBC:
		{
			//KEY must be 128
			key_setting = LED_KEY_128;
			break;
		}
	case LED_BM_SPLIT:
		{
			//KEY must be 64
			key_setting = LED_KEY_64;
			break;
		}
	default:
		break;
	}

	steps = led_steps[key_setting];
	LED::keySize = led_key_sizes[key_setting];
}

void LED::setKey(std::array<uint8_t, 16> config_key)
{
	SPuf::key = config_key;
}

void LED::addRoundKey(block_t s, block_t sk)
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			//i-th row
			s[i][j] ^= sk[i][j];
			s[i][j] = NIBBLE(s[i][j]);
		}
	}
}

void LED::addConstant(block_t s, int r)
{
	s[0][0] = NIBBLE( s[0][0] ^ NIBBLE((8*keySize) >> 4)			);
	s[1][0] = NIBBLE( s[1][0] ^ NIBBLE((8*keySize) >> 4)	^ 0x1U	);
	s[2][0] = NIBBLE( s[2][0] ^ NIBBLE((8*keySize)) 		^ 0x2U	);
	s[3][0] = NIBBLE( s[3][0] ^ NIBBLE((8*keySize)) 		^ 0x3U	);

	s[0][1] = NIBBLE( s[0][1] ^ ((led_rcon[r] >> 3) 	& 0x7)	);
	s[1][1] = NIBBLE( s[1][1] ^ ( led_rcon[r]			& 0x7)	);
	s[2][1] = NIBBLE( s[2][1] ^ ((led_rcon[r] >> 3) 	& 0x7)	);
	s[3][1] = NIBBLE( s[3][1] ^ ( led_rcon[r]			& 0x7)	);
}

void LED::subCells(block_t s)
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			s[i][j] = led_sbox[NIBBLE(s[i][j])];
		}
	}
}

void LED::shiftRows(block_t s)
{
	uint8_t s_0 = 0;

	// for every row starting from index 1
	for(int i = 1; i < 4; i++)
	{
		// perform left shift i times
		for(int n_shifts = 0; n_shifts < i; n_shifts++)
		{
			// this is a single left shift
			s_0 = s[i][0];
			for(int j = 0; j < 3; j++)
			{
				s[i][j] = NIBBLE(s[i][j + 1]);
			}
			s[i][3] = s_0;
		}
	}
}

void LED::mixColumnsSerial(block_t s)
{
	uint8_t tmp[4] = {0};
	uint8_t sum = 0;

	for(int j = 0; j < 4; j++)
	{
		for(int i = 0; i < 4; i++)
		{
			sum = 0;
			for(int k = 0; k < 4; k++)
			{
				sum ^= multiply(MDS[i][k], s[k][j]);
			}
			tmp[i] = sum;
		}

		for(int i = 0; i < 4; i++)
		{
			s[i][j] = NIBBLE(tmp[i]);
		}
	}
}

uint8_t LED::multiply(uint8_t a, uint8_t b)
{
	const uint8_t p = 0x3;
	uint8_t x = a;
	uint8_t ret = 0;
	
	for(int i = 0; i < 4; i++)
	{
		if((b >> i) & 1) 
		{
			ret ^= x;
		}

		if( x & 0x8 )
		{
			x <<= 1;
			x ^= p;
		}
		else
		{
			x <<= 1;
		}
	}
	return NIBBLE(ret);
}

void LED::keyExpansion(uint8_t k[], block_t e[])
{
	// q-th subkey
	for(int q = 0; q < steps+1; q++)
	{
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j+=2)
			{
				e[q][i][j] = NIBBLE(k[i*2 + j/2] >> 4);
				e[q][i][j+1] = NIBBLE(k[i*2 + j/2]);
			}
		}
	}
}

void LED::encrypt(const uint8_t in[8], uint8_t out[8], uint8_t key[])
{
	block_t state = {0};
	block_t *expKey = new block_t[steps + 1];

	// copy plaintext into state by rows
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j+=2)
		{
			state[i][j] = NIBBLE(in[i*2 + j/2] >> 4);
			state[i][j+1] = NIBBLE(in[i*2 + j/2]);
		}
	}

	// generate nibble-based key
	keyExpansion(key,expKey);

	// steps 0 - s-1
	for(int s = 0; s < steps; s++)
	{	
		addRoundKey(state,expKey[s]);
		
		// step block
		for(int r = 0; r < 4; r++)
		{
			addConstant(state,s*4 + r);
			subCells(state);
			shiftRows(state);
			mixColumnsSerial(state);
		}
	}

	// step s
	addRoundKey(state,expKey[steps]);

	// copy output
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j+=2)
		{
			out[i*2 + j/2] = (state[i][j] << 4) | (state[i][j+1]);
		}
	}

	// deallocate everything
	delete[] expKey;
}

void LED::query(const std::array<uint8_t, 16>* const challenge, std::array<uint8_t, 16>* const response)
{
	switch (blockMode)
	{

	case LED_BM_ECB:
		{
			// KEY must be 128
			LED::encrypt(challenge->data(),response->data(),SPuf::key.data());
			LED::encrypt(challenge->data()+8,response->data()+8,SPuf::key.data());
			break;
		}

	case LED_BM_CBC:
		{
			// KEY must be 128
			srand (time(NULL));
			std::array<uint8_t,8> init_vector = {0};

			for (int i = 0; i < init_vector.size(); i++)
			{
				init_vector[i] = challenge->data()[i] ^ (uint8_t)(rand() % (0x100));
			}

			LED::encrypt(init_vector.data(),response->data(),SPuf::key.data());

			for (int i = 0; i < init_vector.size(); i++)
			{
				init_vector[i] = challenge->data()[i+8] ^ response->data()[i];
			}

			LED::encrypt(init_vector.data(),response->data()+8,SPuf::key.data());
			
			break;
		}
	
	case LED_BM_SPLIT:
	default:
		{
			// KEY must be 64
			LED::encrypt(challenge->data(),response->data(),SPuf::key.data());
			LED::encrypt(challenge->data()+8,response->data()+8,SPuf::key.data()+8);
			break;
		}
	}
}