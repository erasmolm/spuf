//
// Created by Erasmo La Montagna on 13/09/2020.
//

#ifndef SPUF_PRESENT_H
#define SPUF_PRESENT_H

#include "spuf.h"

typedef enum
{
	PRESENT_BM_NONE,
	PRESENT_BM_ECB,
	PRESENT_BM_CBC,
	PRESENT_BM_CBC_TEST
}present_block_mode_t;

/**
 * Lookup table for PRESENT substitution process.
 */
static uint8_t const present_g_sbox[] =
{
	0x0CU, 0x05U, 0x06U, 0x0BU,
	0x09U, 0x00U, 0x0AU, 0x0DU,
	0x03U, 0x0EU, 0x0FU, 0x08U,
	0x04U, 0x07U, 0x01U, 0x02u
};

/**
 * Lookup table for PRESENT inverse substitution process.
 */
static uint8_t const present_g_sbox_inv[] =
{
	0x05U, 0x0EU, 0x0FU, 0x08U,
	0x0CU, 0x01U, 0x02U, 0x0DU,
	0x0BU, 0x04U, 0x06U, 0x03U,
	0x00U, 0x07U, 0x09U, 0x0Au
};

class Present : public SPuf::SPuf
{
private:
	present_block_mode_t blockMode;

	void present_add_key (uint8_t *p_text, uint8_t const *p_key);
	void present_encrypt_permutation (uint8_t *p_text);
	void present_substitution(uint8_t * p_text);
	void present_update_encrypt_key (uint8_t * p_key, uint8_t round_counter);
	void present_rotate_key_left (uint8_t *p_key);

public:
	Present(present_block_mode_t bm, const std::array<uint8_t, 16> keyConfig);

	void Encrypt(uint8_t *p_text, uint8_t const *p_key);
	void setKey(std::array<uint8_t, 16> keyConfig);
	void query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response);
};

#endif // SPUF_PRESENT_H