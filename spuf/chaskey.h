//
// Created by Erasmo La Montagna on 20/06/2020.
//

#ifndef SPUF_CHASKEY_H
#define SPUF_CHASKEY_H

#include "spuf.h"

#define ROTL(x,b) (uint32_t)( ((x) >> (32 - (b))) | ( (x) << (b)) )

#define U8TO32_LE(p)              \
	(((uint32_t)((p)[0])) |       \
	 ((uint32_t)((p)[1]) << 8) |  \
	 ((uint32_t)((p)[2]) << 16) | \
	 ((uint32_t)((p)[3]) << 24))

#define ROUND                  \
	do                         \
	{                          \
		v[0] += v[1];          \
		v[1] = ROTL(v[1], 5);  \
		v[1] ^= v[0];          \
		v[0] = ROTL(v[0], 16); \
		v[2] += v[3];          \
		v[3] = ROTL(v[3], 8);  \
		v[3] ^= v[2];          \
		v[0] += v[3];          \
		v[3] = ROTL(v[3], 13); \
		v[3] ^= v[0];          \
		v[2] += v[1];          \
		v[1] = ROTL(v[1], 7);  \
		v[1] ^= v[2];          \
		v[2] = ROTL(v[2], 16); \
	} while (0)

#define PERMUTE \
	ROUND;      \
	ROUND;      \
	ROUND;      \
	ROUND;      \
	ROUND;      \
	ROUND;      \
	ROUND;      \
	ROUND;


#define TIMESTWO(out, in)                       \
	do                                          \
	{                                           \
		out[0] = (in[0] << 1) ^ C[in[3] >> 31]; \
		out[1] = (in[1] << 1) | (in[0] >> 31);  \
		out[2] = (in[2] << 1) | (in[1] >> 31);  \
		out[3] = (in[3] << 1) | (in[2] >> 31);  \
	} while (0)

#define PAD(array, idx, num) ((num == idx) ? ((uint32_t)0x01) : ((uint32_t)array[idx]))

typedef enum
{
	CHASKEY_8,
	CHASKEY_12,
	CHASKEY_LTS
}chaskey_mode_t;

static const volatile uint32_t C[2] = {0x00, 0x87};

static const uint8_t chaskey_rounds[] = {8, 12, 16};

class Chaskey: public SPuf::SPuf
{

private:

	uint8_t rounds;

public:

    Chaskey();

    Chaskey(chaskey_mode_t mode);

    Chaskey(chaskey_mode_t mode, const std::array<uint8_t, 16> key);

	void setKey(std::array<uint8_t, 16> config_key);

	void encrypt(uint32_t v[4], uint32_t key[4]);

	void query(const std::array<uint8_t, 16>* const challenge, std::array<uint8_t, 16>* const response);
};


#endif //SPUF_CHASKEY_H