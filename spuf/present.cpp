#include "present.h"
#include "time.h"

#define PRESENT_ROTATE_BUFF_SIZE_LEFT 		5U
#define PRESENT_ROTATION_POINT_LEFT 		3U
#define PRESENT_UNROTATED_BLOCK_COUNT_LEFT 	4U
#define PRESENT_ROTATION_LSB_OFFSET 		5U
#define PRESENT_ROTATION_MSB_OFFSET 		4U

#define PRESENT_CRYPT_BIT_SIZE 			64U
#define PRESENT_CRYPT_SIZE 				(PRESENT_CRYPT_BIT_SIZE / 8u)
#define PRESENT_PERMUTATION_BUFF_SIZE	(PRESENT_CRYPT_BIT_SIZE / 16u)

#define PRESENT_KEY_BIT_SIZE 	128U
#define PRESENT_KEY_SIZE 		(PRESENT_KEY_BIT_SIZE / 8U)
#define PRESENT_KEY_OFFSET 		(PRESENT_KEY_SIZE - PRESENT_CRYPT_SIZE)
#define PRESENT_KEY_BLOCK_SIZE 	(PRESENT_KEY_BIT_SIZE / 16u)


#define PRESENT_ROUND_COUNT 	31U
#define PRESENT_ROUND_COUNT_MIN 1U
#define PRESENT_ROUND_COUNT_MAX 31U

#define BIT(x) 					(1u << (x))
#define BITVAL(var, bit) 		(((var) & BIT(bit)) >> (bit))


Present::Present(present_block_mode_t bm, const std::array<uint8_t, 16> keyConfig) : SPuf(keyConfig)
{
	Present::blockMode = bm;
}

void Present::setKey(std::array<uint8_t, 16> keyConfig)
{
	SPuf::key = keyConfig;
}

void Present::present_encrypt_permutation(uint8_t *p_text)
{
    uint16_t buff[PRESENT_PERMUTATION_BUFF_SIZE] = {0u};
    uint8_t  bit                                 = 0u;
    uint8_t  byte                                = 0u;

    // ASSERT(NULL != p_text);

    /*
     * Every new 16-bit block has two bits from every bytes of the old text
     * block. Even bits are from low and odd bits are from high nibbles.
     * In every step of the loop, bit values are picked from related bytes.
     * For detailed explanation of the bit positioning, see the article.
     */
    while (byte < PRESENT_CRYPT_SIZE)
    {
        buff[0] |= BITVAL(p_text[byte], 0u) << (2 * bit);
        buff[0] |= BITVAL(p_text[byte], 4u) << (2 * bit + 1);

        buff[1] |= BITVAL(p_text[byte], 1u) << (2 * bit);
        buff[1] |= BITVAL(p_text[byte], 5u) << (2 * bit + 1);

        buff[2] |= BITVAL(p_text[byte], 2u) << (2 * bit);
        buff[2] |= BITVAL(p_text[byte], 6u) << (2 * bit + 1);

        buff[3] |= BITVAL(p_text[byte], 3u) << (2 * bit);
        buff[3] |= BITVAL(p_text[byte], 7u) << (2 * bit + 1);

        bit++;
        byte++;
    }

    /*
     * Copy the new value to the cipher block.
     */
    memcpy(p_text, buff, PRESENT_CRYPT_SIZE);
}

void Present::present_substitution(uint8_t *p_text)
{
    uint8_t const * p_sbox;
    uint8_t         high_nibble;
    uint8_t         low_nibble;
    uint8_t         byte;

    // ASSERT(NULL != p_text);

	p_sbox = present_g_sbox;

    /*
     * Replace all the bytes in the text block.
     */
    for (byte = 0u; byte < PRESENT_CRYPT_SIZE; byte++)
    {
        high_nibble = (p_text[byte] & 0xF0u) >> 4;
        high_nibble = p_sbox[high_nibble];

        low_nibble = p_text[byte] & 0x0Fu;
        low_nibble = p_sbox[low_nibble];

        p_text[byte] = (high_nibble << 4) | low_nibble;
    }
}

void Present::present_add_key(uint8_t * p_text, uint8_t const * p_key)
{
    uint8_t byte;

    // ASSERT(NULL != p_text);
    // ASSERT(NULL != p_key);

    /*
     * Move key pointer to the start byte of the key part that specified in
     * the article. For further information, see article's section 3.
     */
    p_key += PRESENT_KEY_OFFSET;

    /*
     * Adding key is simply logic XOR operation.
     */
    for (byte = 0u; byte < PRESENT_CRYPT_SIZE; byte++)
    {
        p_text[byte] = p_text[byte] ^ p_key[byte];
    }
}

void Present::present_rotate_key_left(uint8_t *p_key)
{
    uint16_t   buff[PRESENT_ROTATE_BUFF_SIZE_LEFT];
    uint16_t * p_block;
    uint8_t    block;

    uint8_t const rotation_point   = PRESENT_ROTATION_POINT_LEFT;
    uint8_t const unrotated_blocks = PRESENT_UNROTATED_BLOCK_COUNT_LEFT;
    uint8_t const lsb_offset       = PRESENT_ROTATION_LSB_OFFSET;
    uint8_t const msb_offset       = PRESENT_ROTATION_MSB_OFFSET;

    // ASSERT(NULL != p_key);

    p_block = (uint16_t *)p_key;

    /*
     * Fill the buffer with values that changes during the first loop.
     */
    for (block = 0u; block < PRESENT_ROTATE_BUFF_SIZE_LEFT; block++)
    {
        buff[block] = p_block[block];
    }

    /*
     * Place the LSB 3-bit and the MSB 13-bit of the related blocks to the
     * new place index until the rotation point.
     */
    for (block = 0u; block < rotation_point; block++)
    {
        p_block[block] = (p_block[block + lsb_offset] << 13) \
                         | (p_block[block + msb_offset] >> 3);
    }

    /*
     * Place the rotation point value by hand. Since the first block of the
     * key has changed during the first loop, use the buffer value.
     */
    p_block[rotation_point] = (buff[0] << 13) | (p_block[PRESENT_KEY_BLOCK_SIZE - 1] >> 3);

    /*
     * Fill the remain blocks with buffer values.
     */
    for (block = 0u; block < unrotated_blocks; block++)
    {
        p_block[block + 4] = (buff[block + 1] << 13) | (buff[block] >> 3);
    }
}

void Present::present_update_encrypt_key(uint8_t *p_key, uint8_t round_counter)
{
    uint8_t high_nibble;
    uint8_t low_nibble;

    // ASSERT(NULL != p_key);
    // ASSERT(round_counter >= PRESENT_ROUND_COUNT_MIN);
    // ASSERT(round_counter <= PRESENT_ROUND_COUNT_MAX);

    /*
     * Rotate the key to the left as first step of the key scheduling.
     */
    present_rotate_key_left(p_key);

    /*
     * Substitute the MSB high nibble of the key.
     */
    high_nibble = (p_key[PRESENT_KEY_SIZE - 1] & 0xF0u) >> 4;
    high_nibble = present_g_sbox[high_nibble];

    low_nibble = p_key[PRESENT_KEY_SIZE - 1] & 0x0Fu;

    /*
     * Substitute the MSB low nibble if 128-bit key is used.
     */
    low_nibble = present_g_sbox[low_nibble];

    p_key[PRESENT_KEY_SIZE - 1] = (high_nibble << 4) | low_nibble;

    /*
     * XOR the from 62th to 66th bits with the round counter.
     */
    p_key[8] ^= round_counter >> 2;
    p_key[7] ^= round_counter << 6;
}

void Present::Encrypt(uint8_t *p_text, uint8_t const *p_key)
{
	uint8_t subkey[PRESENT_KEY_SIZE];
	uint8_t round = 1u;

	// ASSERT(NULL != p_text);
	// ASSERT(NULL != p_key);

	/* 
	 * Copy the key into a buffer to keep original value unchanged during
	 * the encryption process.
	 */
	memcpy(subkey, p_key, PRESENT_KEY_SIZE);

	/*
	 * Main loop of the PRESENT encryption algorithm.
	 */
	while (round <= PRESENT_ROUND_COUNT)
	{
		present_add_key(p_text, subkey);
		present_substitution(p_text);
		present_encrypt_permutation(p_text);


		present_update_encrypt_key(subkey, round);

		round++;
	};

	/*
	 * Add the last subkey to finish the process.
	 */
	present_add_key(p_text, subkey);
}

void Present::query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response)
{	
	switch (Present::blockMode)
	{
		case PRESENT_BM_NONE:
		{
			// plaintext 64 bit, key 128 bit 
			uint8_t ptext[8];

			for(int i = 0; i < 8; i++)
			{
				ptext[i] = (*challenge)[i];
			}

			Encrypt(ptext,SPuf::key.data());

			for(int i = 0; i < 8; i++)
			{
				(*response)[i] = ptext[i];
			}
			break;
		}

		case PRESENT_BM_ECB:
		{
			uint8_t ptext[16];

			for(int i = 0; i < 16; i++)
			{
				ptext[i] = (*challenge)[i];
			}

			Encrypt(&ptext[0],SPuf::key.data());
			Encrypt(&ptext[8],SPuf::key.data());
			
			for(int i = 0; i < 16; i++)
			{
				(*response)[i] = ptext[i];
			}

			break;
		}

		case PRESENT_BM_CBC:
		{
			// Initialize IV
			srand (time(NULL));

			// use response vector as temporary array
			for (int i = 0; i < 8; i++)
			{
				(*response)[i] = (*challenge)[i] ^ (uint8_t)(rand() % (0x100));
			}

			Encrypt(response->data(),SPuf::key.data());

			// prepare 2nd half of plaintext
			for(int i = 0; i < 8; i++)
			{
				(*response)[8 + i] = (*response)[i] ^ (*challenge)[8 + i];
			}

			Encrypt(&(*response)[8],SPuf::key.data());
			
			break;
		}

		case PRESENT_BM_CBC_TEST:
		{
			// IV is considered null

			// use response vector as temporary array
			for (int i = 0; i < 8; i++)
			{
				(*response)[i] = (*challenge)[i];
			}

			Encrypt(response->data(),SPuf::key.data());

			// prepare 2nd half of plaintext
			for(int i = 0; i < 8; i++)
			{
				(*response)[8 + i] = (*response)[i] ^ (*challenge)[8 + i];
			}

			Encrypt(&(*response)[8],SPuf::key.data());
			
			break;
		}
		default:
		{
			std::cerr << "\nNo Present block mode selected\n";
			break;
		}
	}
	
	
	
	

}