//
// Created by Erasmo La Montagna on 11/09/2020.
//

#include "zorro.h"

Zorro::Zorro(const std::array<uint8_t, 16> keyConfig) : SPuf()
{
	SPuf::key = keyConfig;

}

void Zorro::setKey(std::array<uint8_t, 16> keyConfig)
{
	SPuf::key = keyConfig;
}

void Zorro::Encrypt(uint8_t *state, uint8_t *key)
{
	/* Key Whitening - 6 x 4 Rounds of Zorro */
	int round = 1;

	/* Key Whitening */
	for(int i = 0; i < 16; i++)
	{
		state[i] ^= key[i];
	}

	/* 6 x 4 Rounds of Zorro */
	for(int i = 0; i < 6; i++)
	{
		zorroFourRoundEnc(state, key, round);
		round += 4;
	}
}

void Zorro::query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response)
{
	uint8_t pt[16] = {0};

	for(int i = 0; i < 16; i++)
	{
		pt[i] = (*challenge)[i];
	}

	Encrypt(pt,SPuf::key.data());

	for(int i = 0; i < 16; i++)
	{
		(*response)[i] = pt[i];
	}
}

uint8_t Zorro::mulGaloisField2_8(uint8_t a, uint8_t b)
{
	uint8_t p = 0;
	uint8_t hi_bit_set;

	for(uint8_t counter = 0; counter < 8; counter++)
	{
		if ((b & 1) == 1)
		{
			p ^= a;
		}
		hi_bit_set = (a & 0x80);
		a <<= 1;
		if (hi_bit_set == 0x80)
		{
			a ^= 0x1b;
		}
		b >>= 1;
	}
	return p;
}

void Zorro::mixColumn(uint8_t *column)
{
	uint8_t cpy[4];

	for(int i = 0; i < 4; i++)
	{
		cpy[i] = column[i];
	}
	column[0] = mulGaloisField2_8(cpy[0], 2) ^
				mulGaloisField2_8(cpy[1], 3) ^
				mulGaloisField2_8(cpy[2], 1) ^
				mulGaloisField2_8(cpy[3], 1);
	column[1] = mulGaloisField2_8(cpy[0], 1) ^
				mulGaloisField2_8(cpy[1], 2) ^
				mulGaloisField2_8(cpy[2], 3) ^
				mulGaloisField2_8(cpy[3], 1);
	column[2] = mulGaloisField2_8(cpy[0], 1) ^
				mulGaloisField2_8(cpy[1], 1) ^
				mulGaloisField2_8(cpy[2], 2) ^
				mulGaloisField2_8(cpy[3], 3);
	column[3] = mulGaloisField2_8(cpy[0], 3) ^
				mulGaloisField2_8(cpy[1], 1) ^
				mulGaloisField2_8(cpy[2], 1) ^
				mulGaloisField2_8(cpy[3], 2);
}

void Zorro::invMixColumn(uint8_t *column)
{
	uint8_t cpy[4];

	for(int i = 0; i < 4; i++)
	{
		cpy[i] = column[i];
	}
	column[0] = mulGaloisField2_8(cpy[0], 14) ^
				mulGaloisField2_8(cpy[1], 11) ^
				mulGaloisField2_8(cpy[2], 13) ^
				mulGaloisField2_8(cpy[3], 9);
	column[1] = mulGaloisField2_8(cpy[0], 9) ^
				mulGaloisField2_8(cpy[1], 14) ^
				mulGaloisField2_8(cpy[2], 11) ^
				mulGaloisField2_8(cpy[3], 13);
	column[2] = mulGaloisField2_8(cpy[0], 13) ^
				mulGaloisField2_8(cpy[1], 9) ^
				mulGaloisField2_8(cpy[2], 14) ^
				mulGaloisField2_8(cpy[3], 11);
	column[3] = mulGaloisField2_8(cpy[0], 11) ^
				mulGaloisField2_8(cpy[1], 13) ^
				mulGaloisField2_8(cpy[2], 9) ^
				mulGaloisField2_8(cpy[3], 14);
}

void Zorro::zorro_MixColumns(uint8_t *internBuffer)
{
	uint8_t column[4];

	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			column[j] = internBuffer[(i * 4) + j];
		}
		mixColumn(column);
		for(int j = 0; j < 4; j++)
		{
			internBuffer[(i * 4) + j] = column[j];
		}
	}
}

void Zorro::zorro_InvMixColumns(uint8_t *internBuffer)
{
	uint8_t column[4];
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			column[j] = internBuffer[(i * 4) + j];
		}
		invMixColumn(column);
		for(int j = 0; j < 4; j++)
		{
			internBuffer[(i * 4) + j] = column[j];
		}
	}
}

void Zorro::zorroOneRoundEnc(uint8_t *state, uint8_t round)
{
/* SubBytes - AddConstant - ShiftRows - MixColumns */

	/* SubBytes */
	state[0] = s[state[0]];
	state[4] = s[state[4]];
	state[8] = s[state[8]];
	state[12] = s[state[12]];

	/* Add Constant */
	state[0] = state[0] ^ round;
	state[4] = state[4] ^ round;
	state[8] = state[8] ^ round;
	state[12] = state[12] ^ (round << 3);

	/* Shift Rows */
	uint8_t tmp = state[1];
	state[1] = state[5];
	state[5] = state[9];
	state[9] = state[13];
	state[13] = tmp;

	tmp = state[2];
	state[2] = state[10];
	state[10] = tmp;
	tmp = state[6];
	state[6] = state[14];
	state[14] = tmp;

	tmp = state[3];
	state[3] = state[15];
	state[15] = state[11];
	state[11] = state[7];
	state[7] = tmp;

	/* MixColumn */
	zorro_MixColumns(state);
};

void Zorro::zorroFourRoundEnc(uint8_t *state, uint8_t *key, uint8_t round)
{
	/* 4 Rounds - KeyAddition */
	for(int i = 0; i < 4; i++)
	{
		zorroOneRoundEnc(state, round);
		round++;
	}

	/* Key addition */
	for(int i = 0; i < 16; i++)
	{
		state[i] ^= key[i];
	}
};