//
// Created by Erasmo La Montagna on 09/07/2020.
//

#ifndef SPUF_SPECK_H
#define SPUF_SPECK_H

#include "spuf.h"

#define ROTL32(x, r) (((x) << (r)) | (x >> (32 - (r))))
#define ROTR32(x, r) (((x) >> (r)) | ((x) << (32 - (r))))
#define ROTL64(x, r) (((x) << (r)) | (x >> (64 - (r))))
#define ROTR64(x, r) (((x) >> (r)) | ((x) << (64 - (r))))

// SPECK64/128
#define ER32(x, y, k) (x = ROTR32(x, 8), x += y, x ^= k, y = ROTL32(y, 3), y ^= x) 
#define DR32(x, y, k)(y ^= x, y = ROTR32(y, 3), x ^= k, x -= y, x = ROTL32(x, 8))

// SPECK128/128
#define ER64(x, y, k) (x = ROTR64(x, 8), x += y, x ^= k, y = ROTL64(y, 3), y ^= x)
#define DR64(x, y, k) (y ^= x, y = ROTR64(y, 3), x ^= k, x -= y, x = ROTL64(x, 8))

typedef enum
{
	SPECK_BLOCK_64,
	SPECK_BLOCK_128,
}speck_block_size_t;

typedef enum
{
	SPECK_BM_NONE,
	SPECK_BM_ECB,
	SPECK_BM_CBC
}speck_block_mode_t;

static const uint8_t speck_block_size[] = {8, 16};

class Speck : public SPuf::SPuf
{
private:
	uint8_t blockSize;
	speck_block_mode_t blockMode;

	void BytesToWords64(const uint8_t bytes[], uint64_t words[], int numbytes);
	void BytesToWords32(const uint8_t bytes[], uint32_t words[], int numbytes);

	void Words32ToBytes(uint32_t words[], uint8_t bytes[], int numwords);
	void Words64ToBytes(uint64_t words[], uint8_t bytes[], int numwords);

	void KeySchedule64128(uint32_t K[], uint32_t rk[]);
	void KeySchedule128128(uint64_t K[], uint64_t rk[]);


public:
	Speck(speck_block_size_t bs, speck_block_mode_t mode);
	Speck(speck_block_size_t bs, speck_block_mode_t mode, const std::array<uint8_t, 16> keyConfig);

	void setKey(std::array<uint8_t, 16> config_key);

	void Encrypt64128(uint32_t Pt[], uint32_t Ct[], uint32_t rk[]);
	void Encrypt128128(uint64_t Pt[], uint64_t Ct[], uint64_t rk[]);

	void query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response);
};

#endif //SPUF_SPECK_H