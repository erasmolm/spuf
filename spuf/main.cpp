#include <iostream>
#include <array>
#include <fstream>
#include <string>
#include "spuf.h"

// Ciphers includes
#include "simon.h"
#include "aes.h"
#include "led.h"
#include "chaskey.h"
#include "speck.h"
#include "sparx.h"
#include "zorro.h"
#include "present.h"
#include "xorpuf.h"

// List of all implemented ciphers
typedef enum
{
	xorpuf,
	simon,
	aes,
	led_split,
	led_cbc,
	led_ecb,
	chaskey,
	speck,
	speck_cbc,
	speck_ecb,
	sparx,
	sparx_cbc,	
	sparx_ecb,
	zorro,
	present_ecb,
	present_cbc
} cipher_t;

static const cipher_t cipherList[] = 
{
	xorpuf,
	simon,
	aes,
	led_split,
	led_cbc,
	led_ecb,
	chaskey,
	speck,
	speck_cbc,
	speck_ecb,
	sparx,
	sparx_cbc,	
	sparx_ecb,
	zorro,
	present_ecb,
	present_cbc
};

const std::string keyspath = "../resources/keys.txt";
const std::string challengespath = "../resources/challenges.txt";
std::string responsespath = "";

static SPuf *buildPseudoPuf(
	const cipher_t cipher,
	const std::array<uint8_t, 16> key)
{
	SPuf *spuf;

	switch (cipher)
	{
		case xorpuf:
		{
			spuf = new Xorpuf();
			responsespath.assign("./responses_xorpuf.txt");
			break;
		}

		case simon:
		{
			spuf = new Simon(key);
			responsespath.assign("./responses_simon.txt");
			break;
		}

		case aes:
		{
			spuf = new AES(key);
			responsespath.assign("./responses_aes.txt");
			break;
		}

		case led_split:
		{
			spuf = new LED(LED_BM_SPLIT, key);
			responsespath.assign("./responses_led_split.txt");
			break;
		}		
		
		case led_cbc:
		{
			spuf = new LED(LED_BM_CBC, key);
			responsespath.assign("./responses_led_cbc.txt");
			break;
		}		
		
		case led_ecb:
		{
			spuf = new LED(LED_BM_ECB, key);
			responsespath.assign("./responses_led_ecb.txt");
			break;
		}

		case chaskey:
		{
			spuf = new Chaskey(CHASKEY_LTS, key);
			responsespath.assign("./responses_chaskey.txt");
			break;
		}

		case speck:
		{
			spuf = new Speck(SPECK_BLOCK_128, SPECK_BM_NONE, key);
			responsespath.assign("./responses_speck.txt");
			break;
		}

		case speck_cbc:
		{
			spuf = new Speck(SPECK_BLOCK_64, SPECK_BM_CBC, key);
			responsespath.assign("./responses_speck_cbc.txt");
			break;
		}

		case speck_ecb:
		{
			spuf = new Speck(SPECK_BLOCK_64, SPECK_BM_ECB, key);
			responsespath.assign("./responses_speck_ecb.txt");
			break;
		}

		case sparx:
		{
			spuf = new Sparx(SPARX_BLOCK_128, SPARX_BM_NONE, key);
			responsespath.assign("./responses_sparx.txt");
			break;
		}

		case sparx_cbc:
		{
			spuf = new Sparx(SPARX_BLOCK_64, SPARX_BM_CBC, key);
			responsespath.assign("./responses_sparx_cbc.txt");
			break;
		}

		case sparx_ecb:
		{
			spuf = new Sparx(SPARX_BLOCK_64, SPARX_BM_ECB, key);
			responsespath.assign("./responses_sparx_ecb.txt");
			break;
		}

		case zorro:
		{
			spuf = new Zorro(key);
			responsespath.assign("./responses_zorro.txt");
			break;
		}

		case present_ecb:
		{
			spuf = new Present(PRESENT_BM_ECB,key);
			responsespath.assign("./responses_present_ecb.txt");
			break;
		}

		case present_cbc:
		{
			spuf = new Present(PRESENT_BM_CBC,key);
			responsespath.assign("./responses_present_cbc.txt");
			break;
		}

		default:
		{
			std::cout << cipher << " is not a valid cipher!\n";
			break;
		}
	}

	return spuf;
};

int main()
{
	std::cout << "..:: SPUF: Strong Physical Unclonable Function ::.." << std::endl;

	std::string kline, cline;
	std::array<uint8_t, 16> challenge;

	std::ifstream keysfile;
	std::ifstream challengesfile;

	// Open keys file
	keysfile.open(keyspath, std::ifstream::in);
	if (!keysfile.is_open())
	{
		std::cerr << "File " << keyspath << " not found" << std::endl;
		std::exit(-1);
	}

	// Open challenges file
	challengesfile.open(challengespath, std::ifstream::in);
	if (!challengesfile.is_open())
	{
		std::cerr << "File " << challengespath << " not found" << std::endl;
		std::exit(-1);
	}

	// For each implemented cipher submit all keys and one challenge
	for (const auto cipher: cipherList)
	{
		std::ofstream responsesfile;
		std::array<uint8_t, 16> key = {0};

		// Build cipher instance and set responsepath
		SPuf *pseudo_puf = buildPseudoPuf((cipher_t)cipher, key);

		// Open response file (specific for the cipher)
		responsesfile.open(responsespath, std::ifstream::out);
		if (!responsesfile.is_open())
		{
			std::cerr << "File " << responsespath << " not found" << std::endl;
			std::exit(-1);
		}

		// For each challenge iterate through all the keys (device instances)
		while (std::getline(challengesfile, cline))
		{
			std::cout << "Got a challenge: " << cline << std::endl;
			for (int i = 0; i < 16; ++i)
			{
				challenge[i] = std::stoi(cline.substr(2 * i, 2), 0, 16) % 255;
			}

			// For each key get response to the given challenge
			while (std::getline(keysfile, kline))
			{
				// std::cout << "Got a key: " << kline << std::endl;
				for (int i = 0; i < 16; ++i)
				{
					key[i] = std::stoi(kline.substr(2 * i, 2), 0, 16) % 255;
				}

				// Set cipher key
				pseudo_puf->setKey(key);

				// Submit challenge e generate response
				std::array<uint8_t, 16> response;
				pseudo_puf->query(&challenge, &response);

				for (int i = 0; i < 16; ++i)
				{
					responsesfile << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)response[i];
				}
				responsesfile << std::endl;
			}
			keysfile.clear();
			keysfile.seekg(0, std::ios::beg);

			// std::flush(responsesfile);
		}
		challengesfile.clear();
		challengesfile.seekg(0, std::ios::beg);

		responsesfile.close();

		delete pseudo_puf;
	}

	challengesfile.close();
	keysfile.close();

	return 0;
}
