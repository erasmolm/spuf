#include "led.h"

int main()
{
	std::array<uint8_t,8> ciphertext = {0};

	std::array<uint8_t,16> plaintext = 
	{
		0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
		0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF
	};
	
	std::array<uint8_t,8> key64 = 
	{
		0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF
	};

	std::array<uint8_t,16> key128 = 
	{
		0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
		0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF
	};

	// LED-64

	LED *led_cipher = new LED();

	led_cipher->encrypt(plaintext.data(),ciphertext.data(),key64.data());

	std::cout << "LED-64:" << std::endl;
	for(int i = 0; i < 8; ++i)
	{
		std::cout<< "0x" << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)ciphertext[i] << " ";
	}
    std::cout << std::endl;

	// 2 X LED-64

	led_cipher->encrypt(plaintext.data(),ciphertext.data(),key128.data());
	led_cipher->encrypt(plaintext.data()+8,ciphertext.data()+8,key128.data()+8);

	std::cout << "2 X LED-64:" << std::endl;
	for(int i = 0; i < 16; ++i)
	{
		std::cout<< "0x" << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)ciphertext[i] << " ";
	}
    std::cout << std::endl;

	// LED-128

	led_cipher = new LED(LED_KEY_128);

	led_cipher->encrypt(plaintext.data(),ciphertext.data(),key128.data());

	std::cout << "LED-128:" << std::endl;
	for(int i = 0; i < 8; ++i)
	{
		std::cout<< "0x" << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)ciphertext[i] << " ";
	}
    std::cout << std::endl;

	return 0;
}