//
// Created by Erasmo La Montagna on 09/07/2020.
//

#include "speck.h"

Speck::Speck(speck_block_size_t bs, speck_block_mode_t mode) : SPuf()
{
	blockSize = speck_block_size[bs];

	if(SPECK_BLOCK_128 == blockSize)
	{
		// ignore mode parameter in case of 128 bit plaintext
		blockMode = SPECK_BM_NONE;
	}
	else
	{
		blockMode = mode;
	}
	
}

Speck::Speck(speck_block_size_t bs, speck_block_mode_t mode, const std::array<uint8_t, 16> keyConfig) : SPuf(keyConfig)
{
	blockSize = speck_block_size[bs];

	if(SPECK_BLOCK_128 == blockSize)
	{
		// ignore mode parameter in case of 128 bit plaintext
		blockMode = SPECK_BM_NONE;
	}
	else
	{
		blockMode = mode;
	}
}

void Speck::setKey(std::array<uint8_t, 16> config_key)
{
	SPuf::key = config_key;
}

void Speck::BytesToWords32(const uint8_t bytes[], uint32_t words[], int numbytes)
{
	int j = 0;
	for (int i = 0; i < numbytes / 4; i++)
	{
		words[i] =
			(uint32_t)bytes[j] |
			((uint32_t)bytes[j + 1] << 8) |
			((uint32_t)bytes[j + 2] << 16) |
			((uint32_t)bytes[j + 3] << 24);
		j += 4;
	}
}

void Speck::BytesToWords64(const uint8_t bytes[], uint64_t words[], int numbytes)
{
	int j = 0;
	for (int i = 0; i < numbytes / 8; i++)
	{
		words[i] =
			(uint64_t)bytes[j] |
			((uint64_t)bytes[j + 1] << 8) |
			((uint64_t)bytes[j + 2] << 16) |
			((uint64_t)bytes[j + 3] << 24) |
			((uint64_t)bytes[j + 4] << 32) |
			((uint64_t)bytes[j + 5] << 40) |
			((uint64_t)bytes[j + 6] << 48) |
			((uint64_t)bytes[j + 7] << 56);
		j += 8;
	}
}

void Speck::Words32ToBytes(uint32_t words[], uint8_t bytes[], int numwords)
{
	int j = 0;
	for (int i = 0; i < numwords; i++)
	{
		bytes[j] = (uint8_t)words[i];
		bytes[j + 1] = (uint8_t)(words[i] >> 8);
		bytes[j + 2] = (uint8_t)(words[i] >> 16);
		bytes[j + 3] = (uint8_t)(words[i] >> 24);
		j += 4;
	}
}

void Speck::Words64ToBytes(uint64_t words[], uint8_t bytes[], int numwords)
{
	int j = 0;
	for (int i = 0; i < numwords; i++)
	{
		bytes[j] = (uint8_t)words[i];
		bytes[j + 1] = (uint8_t)(words[i] >> 8);
		bytes[j + 2] = (uint8_t)(words[i] >> 16);
		bytes[j + 3] = (uint8_t)(words[i] >> 24);
		bytes[j + 4] = (uint8_t)(words[i] >> 32);
		bytes[j + 5] = (uint8_t)(words[i] >> 40);
		bytes[j + 6] = (uint8_t)(words[i] >> 48);
		bytes[j + 7] = (uint8_t)(words[i] >> 56);
		j += 8;
	}
}

void Speck::KeySchedule64128(uint32_t K[], uint32_t rk[])
{
	uint32_t D = K[3], C = K[2], B = K[1], A = K[0];
	for (uint32_t i = 0; i < 27;)
	{
		rk[i] = A;
		ER32(B, A, i++);
		rk[i] = A;
		ER32(C, A, i++);
		rk[i] = A;
		ER32(D, A, i++);
	}
}

void Speck::KeySchedule128128(uint64_t K[], uint64_t rk[])
{
	uint64_t i, B = K[1], A = K[0];
	for (i = 0; i < 31;)
	{
		rk[i] = A;
		ER64(B, A, i++);
	}
	rk[i] = A;
}

void Speck::Encrypt64128(uint32_t Pt[], uint32_t Ct[], uint32_t rk[])
{
	Ct[0] = Pt[0];
	Ct[1] = Pt[1];

	for (uint32_t i = 0; i < 27;)
	{
		ER32(Ct[1], Ct[0], rk[i++]);
	}
}

void Speck::Encrypt128128(uint64_t Pt[], uint64_t Ct[], uint64_t rk[])
{
	uint64_t i;
	Ct[0] = Pt[0];
	Ct[1] = Pt[1];

	for (i = 0; i < 32;)
	{
		ER64(Ct[1], Ct[0], rk[i++]);
	}
}

void Speck::query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response)
{
	if(8 == blockSize)
	{
		switch (blockMode)
		{
		case SPECK_BM_ECB:
			{
				uint32_t pt[4];
				uint32_t ct[4];
				uint32_t K[4];
				uint32_t rk[27];

				// key is always 128 bits
				BytesToWords32(SPuf::key.data(), K, 16);
				KeySchedule64128(K, rk);
				
				// split plaintext into to halves
				BytesToWords32(challenge->data(), pt, 8);
				BytesToWords32(challenge->data()+8, &pt[2], 8);

				// encrypt both halves
				Encrypt64128(pt, ct, rk);
				Encrypt64128(&pt[2], &ct[2], rk);

				// convert ct to byte array
				Words32ToBytes(ct, response->data(), 2);
				Words32ToBytes(&ct[2], response->data()+8, 2);
				
				break;
			}

		case SPECK_BM_CBC:
			{
				uint32_t pt[4];
				uint32_t ct[4];
				uint32_t K[4];
				uint32_t rk[27];

				// key is always 128 bits
				BytesToWords32(SPuf::key.data(), K, 16);
				KeySchedule64128(K, rk);

				// convert plaintext to words
				BytesToWords32(challenge->data(), pt, 8);
				BytesToWords32(challenge->data()+8, &pt[2], 8);

				// XOR first half of pt with the initialization vector
				srand (time(NULL));
				pt[0] = pt[0] ^ (uint32_t)(rand() % (0xFFFFFFFF));
				pt[1] = pt[1] ^ (uint32_t)(rand() % (0xFFFFFFFF));

				// encrypt first half
				Encrypt64128(pt, ct, rk);

				// XOR firt half of ct with the second half of pt
				pt[2] = pt[2] ^ ct[0];
				pt[3] = pt[3] ^ ct[1];

				// encrypt second half
				Encrypt64128(&pt[2], &ct[2], rk);

				// convert ct to byte array
				Words32ToBytes(ct, response->data(), 2);
				Words32ToBytes(&ct[2], response->data()+8, 2);

				break;
			}
		
		default:
			std::cerr << "\nNo Speck block mode selected\n";
			break;
		}
		
	}
	else if(16 == blockSize)
	{
		uint64_t K[2];
		uint64_t rk[32];
		uint64_t pt[2];
		uint64_t ct[2];

		BytesToWords64(SPuf::key.data(), K, 16);
		BytesToWords64(challenge->data(), pt, 16);

		KeySchedule128128(K, rk);
		Encrypt128128(pt, ct, rk);

		Words64ToBytes(ct, response->data(), 2);
	}
}