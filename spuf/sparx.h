//
// Created by Erasmo La Montagna on 22/07/2020.
//

#ifndef SPUF_SPARX_H
#define SPUF_SPARX_H

#include "spuf.h"

#define sparx_ROTL(x, n) (((x) << n) | ((x) >> (16 - (n))))
#define sparx_SWAP(x, y) tmp = x; x = y; y = tmp

#define NSTEPS	8
#define KSIZE	4

typedef enum
{
	SPARX_BLOCK_64,
	SPARX_BLOCK_128,
}sparx_block_size_t;

typedef enum
{
	SPARX_BM_NONE,
	SPARX_BM_ECB,
	SPARX_BM_CBC
}sparx_block_mode_t;

static const uint8_t sparx_block_size[] = {8, 16};
static const uint8_t sparx_n_branches[] = {2, 4};
static const uint8_t sparx_r_steps[] = {3, 4};


class Sparx : public SPuf::SPuf
{
private:
	uint8_t blockSize;
	sparx_block_mode_t blockMode;
	uint8_t roundPerSteps;
	uint8_t nBranches;

	void A(uint16_t * l, uint16_t * r);
	void L_2(uint16_t * x);
	void L_4(uint16_t * x);
	void K_perm_64_128(uint16_t * k, uint16_t c);
	void K_perm_128_128(uint16_t * k, uint16_t c);
	void key_schedule(uint16_t **subkeys, uint16_t master_key[]);
	uint16_t merge_8_to_16(uint8_t higher_part, uint8_t lower_part);

public:
	Sparx(sparx_block_size_t bs, sparx_block_mode_t mode);
	Sparx(sparx_block_size_t bs, sparx_block_mode_t mode, const std::array<uint8_t, 16> keyConfig);

	void setKey(std::array<uint8_t, 16> config_key);

	void Encrypt(uint16_t * x, uint16_t **k);
	void query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response);
};

#endif // SPUF_SPARX_H