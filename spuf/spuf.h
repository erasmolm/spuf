//
// Created by Mario Barbareschi on 20/05/2020.®
//

#ifndef SPUF_SPUF_H
#define SPUF_SPUF_H

#include <iostream>
#include <array>
#include <time.h>
#include <random>
#include <iomanip>
#include <climits>
#include <cstring>

class SPuf
{
protected:
	std::array<uint8_t, 16> key;

public:
	SPuf()
	{
		srand(time(NULL));
		for (int i = 0; i < 16; ++i)
		{
			key[i] = rand() % 256;
		}
	}

	SPuf(const std::array<uint8_t, 16> config_key)
	{
		key = config_key;
	}

	virtual void setKey(std::array<uint8_t, 16> config_key) = 0;

	virtual void query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response) = 0;

	static void print_bitstring(std::array<uint8_t, 16> bitstring)
	{
		for (int i = 0; i < 16; ++i)
			std::cout << "0x" << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)bitstring[i] << " ";
		std::cout << std::endl;
	}
};

#endif //SPUF_SPUF_H
