//
// Created by Mario Barbareschi on 21/05/2020.
//

#include "xorpuf.h"

void Xorpuf::query(const std::array<uint8_t, 16>* const challenge, std::array<uint8_t, 16>* const response){
    for(int i = 0; i < 16; ++i) {
        response->at(i) = challenge->at(i)^SPuf::key[i];
    }
}

void Xorpuf::setKey(std::array<uint8_t, 16> config_key)
{
	SPuf::key = config_key;
}