//
// Created by Erasmo La Montagna on 09/06/2020.
//

#ifndef SPUF_LED_H
#define SPUF_LED_H

#include "spuf.h"

#define NIBBLE(X)		(uint8_t)(X & 0xFU)

typedef enum
{
	LED_KEY_64,
	LED_KEY_128,
}led_key_size_t;

typedef enum
{
	LED_BM_SPLIT,
	LED_BM_ECB,
	LED_BM_CBC
}led_block_mode_t;

typedef uint8_t block_t[4][4];

static const uint8_t led_key_sizes[] = {8, 16};

static const uint8_t led_steps[] = {8, 12};

static const uint8_t MDS[4][4] = 
{
	{0x4, 0x1, 0x2, 0x2},
	{0x8, 0x6, 0x5, 0x6},
	{0xB, 0xE, 0xA, 0x9},
	{0x2, 0x2, 0xF, 0xB},
};

static const uint8_t led_sbox[16] =
{
	0xC, 0x5, 0x6, 0xB, 0x9, 0x0, 0xA, 0xD, 
	0x3, 0xE, 0xF, 0x8, 0x4, 0x7, 0x1, 0x2
};

static const uint led_rcon[48] = 
{
	0x01, 0x03, 0x07, 0x0F,
	0x1F, 0x3E, 0x3D, 0x3B,
	0x37, 0x2F, 0x1E, 0x3C,
	0x39, 0x33, 0x27, 0x0E,
	0x1D, 0x3A, 0x35, 0x2B,
	0x16, 0x2C, 0x18, 0x30,
	0x21, 0x02, 0x05, 0x0B,
	0x17, 0x2E, 0x1C, 0x38,
	0x31, 0x23, 0x06, 0x0D,
	0x1B, 0x36, 0x2D, 0x1A,
	0x34, 0x29, 0x12, 0x24,
	0x08, 0x11, 0x22, 0x04
};

class LED: public SPuf::SPuf
{
private:

	uint8_t steps;
	uint8_t keySize;
	led_block_mode_t blockMode;

	void addRoundKey(block_t state, block_t sk);
	void addConstant(block_t state, int round);
	void subCells(block_t state);
	void shiftRows(block_t state);
	void mixColumnsSerial(block_t state);
	uint8_t multiply(uint8_t a, uint8_t b);
	void keyExpansion(uint8_t k[], block_t e[]);

public:

	LED();

	LED(led_key_size_t mode);

	LED(led_block_mode_t mode, const std::array<uint8_t, 16> key);

	void setKey(std::array<uint8_t, 16> config_key);

	void encrypt(const uint8_t in[8], uint8_t out[8], uint8_t key[]);

	void query(const std::array<uint8_t, 16>* const challenge, std::array<uint8_t, 16>* const response);
};

#endif //SPUF_LED_H