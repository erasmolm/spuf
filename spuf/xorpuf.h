//
// Created by Mario Barbareschi on 21/05/2020.
//

#ifndef SPUF_XORPUF_H
#define SPUF_XORPUF_H

#include "spuf.h"

class Xorpuf : public SPuf {
public:
	void setKey(std::array<uint8_t, 16> config_key);

    void query(const std::array<uint8_t, 16>* const challenge, std::array<uint8_t, 16>* const response);

};


#endif //SPUF_XORPUF_H
