//
// Created by Erasmo La Montagna on 25/05/2020.
//

#include "aes.h"

AES::AES() : SPuf()
{
	// AES-128 as default case
	Nr = 10;
	Nk = 4;
	keySize = aes_key_sizes[AES_KEY_128];
}

AES::AES(const std::array<uint8_t, 16> keyConfig) : SPuf(keyConfig)
{
	// AES-128 as default case
	Nr = 10;
	Nk = 4;
	keySize = aes_key_sizes[AES_KEY_128];
}

void AES::setKey(std::array<uint8_t, 16> config_key)
{
	SPuf::key = config_key;
}

void AES::subBytes(state_t s)
{
	uint8_t s_temp = 0;
	for(int i = 0; i < STATE_ROWS; i++)
	{
		for(int j = 0; j < NB; j++)
		{
			s_temp = s[i][j];
			s[i][j] = SBOX_QUERY(s_temp);
		}
	}
}

void AES::shiftRows(state_t s)
{
	uint8_t s_0 = 0;

	// for every row starting from index 1
	for(int i = 1; i < STATE_ROWS; i++)
	{
		// perform left shift n_shift times
		for(int n_shifts = 0; n_shifts < i; n_shifts++)
		{
			// this is a single left shift
			s_0 = s[i][0];
			for(int j = 0; j < NB - 1; j++)
			{
				s[i][j] = s[i][j + 1];
			}
			s[i][NB - 1] = s_0;
		}
	}
}

void AES::mixColumns(state_t s)
{
	uint8_t a[STATE_ROWS] = {0};
	uint8_t b[STATE_ROWS] = {0};
	uint8_t h = 0;

	for(int j = 0; j < NB; j++)
	{
		for(int i = 0; i < STATE_ROWS; i++)
		{
			a[i] = s[i][j];
			h = (uint8_t)((signed char)s[i][j] >> 7);
			b[i] = s[i][j] << 1;
			b[i] ^= 0x1B & h;
		
		}
		// j-th column
		s[0][j] = b[0] ^ a[3] ^ a[2] ^ b[1] ^ a[1]; /* 2 * a0 + a3 + a2 + 3 * a1 */
		s[1][j] = b[1] ^ a[0] ^ a[3] ^ b[2] ^ a[2]; /* 2 * a1 + a0 + a3 + 3 * a2 */
		s[2][j] = b[2] ^ a[1] ^ a[0] ^ b[3] ^ a[3]; /* 2 * a2 + a1 + a0 + 3 * a3 */
		s[3][j] = b[3] ^ a[2] ^ a[1] ^ b[0] ^ a[0]; /* 2 * a3 + a2 + a1 + 3 * a0 */   
	}

}

void AES::addRoundKey(state_t s, word_t* k)
{
	for(int j = 0; j < NB; j++)
	{
		//j-th column
		s[0][j] ^= k[j][0];
		s[1][j] ^= k[j][1];
		s[2][j] ^= k[j][2];
		s[3][j] ^= k[j][3];
	}
}

void AES::keyExpansion(uint8_t k[], word_t* e)
{
	word_t temp = {0};

	// copy key into expanded key
	for(int i = 0; i<KWORD_SIZE; i++)
	{
		e[i][0] = k[i*KWORD_SIZE + 0];
		e[i][1] = k[i*KWORD_SIZE + 1];
		e[i][2] = k[i*KWORD_SIZE + 2];
		e[i][3] = k[i*KWORD_SIZE + 3];
	}

	// loop on words of 4 bytes
	for(int i = 4; i < (NB*(Nr + 1)); i++)
	{
		temp[0] = e[i - 1][0];
		temp[1] = e[i - 1][1];
		temp[2] = e[i - 1][2];
		temp[3] = e[i - 1][3];

		if(0 == (i%4))
		{
			rotWord(temp);
			subWord(temp);
			temp[0] ^= rcon[i/4];
		}

		e[i][0] = e[i - 4][0] ^ temp[0];
		e[i][1] = e[i - 4][1] ^ temp[1];
		e[i][2] = e[i - 4][2] ^ temp[2];
		e[i][3] = e[i - 4][3] ^ temp[3];
	}
}

void AES::rotWord(word_t w)
{
	uint8_t temp = w[0];
	w[0] = w[1];
	w[1] = w[2];
	w[2] = w[3];
	w[3] = temp;
}

void AES::subWord(word_t w)
{
	for(int i = 0; i < KWORD_SIZE; i++)
	{
		w[i] = SBOX_QUERY(w[i]);
	}
}

void AES::encrypt(const uint8_t in[16], uint8_t out[16], uint8_t key[])
{
	state_t s = {0};
	word_t *expKey = new word_t[NB*(Nr + 1)];

	// copy plaintext into state by columns
	for(int i = 0; i < STATE_ROWS; i++)
	{
		for(int j = 0; j < NB; j++)
		{
			s[i][j] = in[i + j*STATE_ROWS];
		}
	}

	keyExpansion(key,expKey);

	// round 0
	addRoundKey(s,&expKey[0]);

	// rounds 1 - Nr-1
	for(int round = 1; round < Nr; round++)
	{
		subBytes(s);
		shiftRows(s);
		mixColumns(s);
		addRoundKey(s,&expKey[round*NB]);
	}

	// last round
	subBytes(s);
	shiftRows(s);
	addRoundKey(s,&expKey[Nr*NB]);

	// copy output
	for(int i = 0; i < STATE_ROWS; i++)
	{
		for(int j = 0; j < NB; j++)
		{
			out[i + j*STATE_ROWS] = s[i][j];
		}
	}	

	// deallocate everything
	delete[] expKey;
}

void AES::query(const std::array<uint8_t, 16>* const challenge, std::array<uint8_t, 16>* const response)
{
	AES::encrypt(challenge->data(),response->data(),SPuf::key.data());
	AES::encrypt(challenge->data()+8,response->data()+8,SPuf::key.data()+8);
}