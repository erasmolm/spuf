//
// Created by Mario Barbareschi on 21/05/2020.
//

#include "simon.h"

void Simon_Encrypt_32(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext, uint8_t *ciphertext)
{

	const uint8_t word_size = 16;
	uint16_t *y_word = (uint16_t *)ciphertext;
	uint16_t *x_word = (((uint16_t *)ciphertext) + 1);

	*y_word = *(uint16_t *)plaintext;
	*x_word = *(((uint16_t *)plaintext) + 1);

	uint16_t *round_key_ptr = (uint16_t *)key_schedule;

	for (uint8_t i = 0; i < round_limit; i++)
	{

		// Shift, AND , XOR ops
		uint16_t temp = (shift_one(*x_word) & shift_eight(*x_word)) ^ *y_word ^ shift_two(*x_word);

		// Feistel Cross
		*y_word = *x_word;

		// XOR with Round Key
		*x_word = temp ^ *(round_key_ptr + i);
	}
}

void Simon_Encrypt_48(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					  uint8_t *ciphertext)
{

	const uint8_t word_size = 24;

	bword_24 intrd = *(bword_24 *)plaintext;
	uint32_t y_word = intrd.data;
	intrd = *((bword_24 *)(plaintext + 3));
	uint32_t x_word = intrd.data;

	for (uint8_t i = 0; i < round_limit; i++)
	{

		// Shift, AND , XOR ops
		uint32_t temp = (shift_one(x_word) & shift_eight(x_word)) ^ y_word ^ shift_two(x_word);

		// Feistel Cross
		y_word = x_word;

		// XOR with Round Key
		x_word = (temp ^ (*((bword_24 *)(key_schedule + (i * 3)))).data) & 0xFFFFFF;
	}
	// Assemble Ciphertext Output Array
	intrd.data = y_word;
	bword_24 *intrd_ptr = (bword_24 *)ciphertext;
	*intrd_ptr = intrd;

	intrd.data = x_word;
	intrd_ptr = (bword_24 *)(ciphertext + 3);
	*intrd_ptr = intrd;
}

void Simon_Encrypt_64(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					  uint8_t *ciphertext)
{

	const uint8_t word_size = 32;
	uint32_t *y_word = (uint32_t *)ciphertext;
	uint32_t *x_word = (((uint32_t *)ciphertext) + 1);
	*y_word = *(uint32_t *)plaintext;
	*x_word = *(((uint32_t *)plaintext) + 1);
	uint32_t *round_key_ptr = (uint32_t *)key_schedule;

	for (uint8_t i = 0; i < round_limit; i++)
	{

		// Shift, AND , XOR ops
		uint32_t temp = (shift_one(*x_word) & shift_eight(*x_word)) ^ *y_word ^ shift_two(*x_word);

		// Feistel Cross
		*y_word = *x_word;

		// XOR with Round Key
		*x_word = temp ^ *(round_key_ptr + i);
	}
}

void Simon_Encrypt_96(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					  uint8_t *ciphertext)
{

	const uint8_t word_size = 48;

	bword_48 intrd = *(bword_48 *)plaintext;
	uint64_t y_word = intrd.data;
	intrd = *((bword_48 *)(plaintext + 6));
	uint64_t x_word = intrd.data;

	for (uint8_t i = 0; i < round_limit; i++)
	{

		// Shift, AND , XOR ops
		uint64_t temp = (shift_one(x_word) & shift_eight(x_word)) ^ y_word ^ shift_two(x_word);

		// Feistel Cross
		y_word = x_word;

		// XOR with Round Key
		x_word = (temp ^ (*((bword_48 *)(key_schedule + (i * 6)))).data) & 0xFFFFFFFFFFFF;
	}
	// Assemble Ciphertext Output Array
	intrd.data = y_word;
	bword_48 *intrd_ptr = (bword_48 *)ciphertext;
	*intrd_ptr = intrd;

	intrd.data = x_word;
	intrd_ptr = (bword_48 *)(ciphertext + 6);
	*intrd_ptr = intrd;
}

void Simon_Encrypt_128(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					   uint8_t *ciphertext)
{

	const uint8_t word_size = 64;
	uint64_t *y_word = (uint64_t *)ciphertext;
	uint64_t *x_word = (((uint64_t *)ciphertext) + 1);
	*y_word = *(uint64_t *)plaintext;
	*x_word = *(((uint64_t *)plaintext) + 1);
	uint64_t *round_key_ptr = (uint64_t *)key_schedule;

	for (uint8_t i = 0; i < round_limit; i++)
	{

		// Shift, AND , XOR ops
		uint64_t temp = (shift_one(*x_word) & shift_eight(*x_word)) ^ *y_word ^ shift_two(*x_word);

		// Feistel Cross
		*y_word = *x_word;

		// XOR with Round Key
		*x_word = temp ^ *(round_key_ptr + i);
	}
}

void Simon_Decrypt_32(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext)
{

	const uint8_t word_size = 16;
	uint16_t *x_word = (uint16_t *)plaintext;
	uint16_t *y_word = ((uint16_t *)plaintext) + 1;
	uint16_t *round_key_ptr = (uint16_t *)key_schedule;

	*x_word = *(uint16_t *)ciphertext;
	*y_word = *(((uint16_t *)ciphertext) + 1);

	for (int8_t i = round_limit - 1; i >= 0; i--)
	{

		// Shift, AND , XOR ops
		uint16_t temp = (shift_one(*x_word) & shift_eight(*x_word)) ^ *y_word ^ shift_two(*x_word);

		// Feistel Cross
		*y_word = *x_word;

		// XOR with Round Key
		*x_word = temp ^ *(round_key_ptr + i);
	}
}

void Simon_Decrypt_48(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext)
{
	const uint8_t word_size = 24;

	bword_24 intrd = *(bword_24 *)ciphertext;
	uint32_t x_word = intrd.data;
	intrd = *((bword_24 *)(ciphertext + 3));
	uint32_t y_word = intrd.data;

	for (int8_t i = round_limit - 1; i >= 0; i--)
	{

		// Shift, AND , XOR ops
		uint32_t temp = (shift_one(x_word) & shift_eight(x_word)) ^ y_word ^ shift_two(x_word);

		// Feistel Cross
		y_word = x_word;

		// XOR with Round Key
		x_word = (temp ^ (*((bword_24 *)(key_schedule + (i * 3)))).data) & 0xFFFFFF;
	}
	// Assemble plaintext Output Array
	intrd.data = x_word;
	bword_24 *intrd_ptr = (bword_24 *)plaintext;
	*intrd_ptr = intrd;

	intrd.data = y_word;
	intrd_ptr = (bword_24 *)(plaintext + 3);
	*intrd_ptr = intrd;
}

void Simon_Decrypt_64(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext)
{

	const uint8_t word_size = 32;
	uint32_t *x_word = (uint32_t *)plaintext;
	uint32_t *y_word = ((uint32_t *)plaintext) + 1;
	uint32_t *round_key_ptr = (uint32_t *)key_schedule;

	*x_word = *(uint32_t *)ciphertext;
	*y_word = *(((uint32_t *)ciphertext) + 1);

	for (int8_t i = round_limit - 1; i >= 0; i--)
	{

		// Shift, AND , XOR ops
		uint32_t temp = (shift_one(*x_word) & shift_eight(*x_word)) ^ *y_word ^ shift_two(*x_word);

		// Feistel Cross
		*y_word = *x_word;

		// XOR with Round Key
		*x_word = temp ^ *(round_key_ptr + i);
	}
}

void Simon_Decrypt_96(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext)
{
	const uint8_t word_size = 48;
	bword_48 intrd = *(bword_48 *)ciphertext;
	uint64_t x_word = intrd.data;
	intrd = *((bword_48 *)(ciphertext + 6));
	uint64_t y_word = intrd.data;

	for (int8_t i = round_limit - 1; i >= 0; i--)
	{

		// Shift, AND , XOR ops
		uint64_t temp = (shift_one(x_word) & shift_eight(x_word)) ^ y_word ^ shift_two(x_word);

		// Feistel Cross
		y_word = x_word;

		// XOR with Round Key
		x_word = (temp ^ (*((bword_48 *)(key_schedule + (i * 6)))).data) & 0xFFFFFFFFFFFF;
	}
	// Assemble Plaintext Output Array
	intrd.data = x_word;
	bword_48 *intrd_ptr = (bword_48 *)plaintext;
	*intrd_ptr = intrd;

	intrd.data = y_word;
	intrd_ptr = (bword_48 *)(plaintext + 6);
	*intrd_ptr = intrd;
}

void Simon_Decrypt_128(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					   uint8_t *plaintext)
{

	const uint8_t word_size = 64;
	uint64_t *x_word = (uint64_t *)plaintext;
	uint64_t *y_word = ((uint64_t *)plaintext) + 1;
	uint64_t *round_key_ptr = (uint64_t *)key_schedule;

	*x_word = *(uint64_t *)ciphertext;
	*y_word = *(((uint64_t *)ciphertext) + 1);

	for (int8_t i = round_limit - 1; i >= 0; i--)
	{

		// Shift, AND , XOR ops
		uint64_t temp = (shift_one(*x_word) & shift_eight(*x_word)) ^ *y_word ^ shift_two(*x_word);

		// Feistel Cross
		*y_word = *x_word;

		// XOR with Round Key
		*x_word = temp ^ *(round_key_ptr + i);
	}
}

void Simon::setKey(std::array<uint8_t, 16> config_key)
{
	SPuf::key = config_key;
	Simon_Init(&my_simon_cipher, cfg_128_128, ECB, config_key.data(), my_IV, my_counter);
}

void Simon::query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response)
{
	Simon::Simon_Encrypt(Simon::my_simon_cipher, challenge->data(), response->data());
}