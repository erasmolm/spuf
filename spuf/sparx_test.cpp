#include "sparx.h"

int main()
{
	// SPARX 128 128

	uint16_t sparx_128_128_key[] = {0x0011, 0x2233, 0x4455, 0x6677, 0x8899, 0xaabb, 0xccdd, 0xeeff};
	uint16_t sparx_128_128_plaintext[] = {0x0123, 0x4567, 0x89ab, 0xcdef, 0xfedc, 0xba98, 0x7654, 0x3210};

	std::array<uint8_t,16> key = {0};
	std::array<uint8_t,16> plaintext = {0};
	std::array<uint8_t,16> ciphertext = {0};

	for (int i = 0; i < 8; i++)
	{
		key[2*i] = (uint8_t)((sparx_128_128_key[i] & 0xFF00) >> 8);
		key[2*i+1] = (uint8_t)(sparx_128_128_key[i] & 0xFF);

		plaintext[2*i] = (uint8_t)((sparx_128_128_plaintext[i] & 0xFF00) >> 8);
		plaintext[2*i+1] = (uint8_t)(sparx_128_128_plaintext[i] & 0xFF);
	}


	SPuf *cipher128_128 = new Sparx(SPARX_BLOCK_128, SPARX_BM_NONE, key);
	cipher128_128->query(&plaintext, &ciphertext);

	std::cout << "SPARX 128 128 plaintext: \t";
	cipher128_128->print_bitstring(plaintext);

	std::cout << "SPARX 128 128 ciphertext: \t";
	cipher128_128->print_bitstring(ciphertext);
	std::cout << std::endl;

	// SPARX 64 128 ECB MODE

	uint16_t sparx_64_128_plaintext[] = 
	{
		0x0123, 0x4567, 0x89ab, 0xcdef
	};

	for (int i = 0; i < 4; i++)
	{
		plaintext[2*i] = (uint8_t)((sparx_64_128_plaintext[i] & 0xFF00) >> 8);
		plaintext[2*i+1] = (uint8_t)(sparx_64_128_plaintext[i] & 0xFF);
		plaintext[8+2*i] = (uint8_t)((sparx_64_128_plaintext[i] & 0xFF00) >> 8);
		plaintext[8+2*i+1] = (uint8_t)(sparx_64_128_plaintext[i] & 0xFF);
	}

	SPuf *cipher64_128 = new Sparx(SPARX_BLOCK_64, SPARX_BM_ECB, key);
	cipher64_128->query(&plaintext, &ciphertext);

	std::cout << "SPARX 64 128 ECB plaintext: \t";
	cipher64_128->print_bitstring(plaintext);

	std::cout << "SPARX 64 128 ECB ciphertext: \t";
	cipher64_128->print_bitstring(ciphertext);
	std::cout << std::endl;

	// SPARX 64 128 CBC MODE

	for (int i = 0; i < 4; i++)
	{
		plaintext[2*i] = (uint8_t)((sparx_64_128_plaintext[i] & 0xFF00) >> 8);
		plaintext[2*i+1] = (uint8_t)(sparx_64_128_plaintext[i] & 0xFF);
		plaintext[8+2*i] = (uint8_t)((sparx_64_128_plaintext[i] & 0xFF00) >> 8);
		plaintext[8+2*i+1] = (uint8_t)(sparx_64_128_plaintext[i] & 0xFF);
	}

	cipher64_128 = new Sparx(SPARX_BLOCK_64, SPARX_BM_CBC, key);
	cipher64_128->query(&plaintext, &ciphertext);

	std::cout << "SPARX 64 128 CBC plaintext: \t";
	cipher64_128->print_bitstring(plaintext);

	std::cout << "SPARX 64 128 CBC ciphertext: \t";
	cipher64_128->print_bitstring(ciphertext);
	std::cout << std::endl;

	delete cipher128_128;
	delete cipher64_128;
}