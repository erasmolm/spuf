#include <iostream>
#include <array>
#include "spuf.h"
#include "aes.h"

int main()
{
	std::array<uint8_t,16> ciphertext;

	std::array<uint8_t,16> plaintext = 
	{
		0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
	};
	
	std::array<uint8_t,16> key = 
	{
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
	};

	SPuf *dummy_puf = new AES(key);

	dummy_puf->query(&plaintext,&ciphertext);

	dummy_puf->print_bitstring(ciphertext);

	return 0;
}