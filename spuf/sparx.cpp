//
// Created by Erasmo La Montagna on 22/07/2020.
//

#include "sparx.h"

Sparx::Sparx(sparx_block_size_t bs, sparx_block_mode_t mode) : SPuf()
{
	blockSize = sparx_block_size[bs];
	roundPerSteps = sparx_r_steps[bs];
	nBranches = sparx_n_branches[bs];

	if(SPARX_BLOCK_128 == blockSize)
	{
		// ignore mode parameter in case of 128 bit plaintext
		blockMode = SPARX_BM_NONE;
	}
	else
	{
		blockMode = mode;
	}
}

Sparx::Sparx(sparx_block_size_t bs, sparx_block_mode_t mode, const std::array<uint8_t, 16> keyConfig) : SPuf(keyConfig)
{
	blockSize = sparx_block_size[bs];
	roundPerSteps = sparx_r_steps[bs];
	nBranches = sparx_n_branches[bs];

	if(SPARX_BLOCK_128 == blockSize)
	{
		// ignore mode parameter in case of 128 bit plaintext
		blockMode = SPARX_BM_NONE;
	}
	else
	{
		blockMode = mode;
	}
}

void Sparx::setKey(std::array<uint8_t, 16> config_key)
{
	SPuf::key = config_key;
}

void Sparx::A(uint16_t * l, uint16_t * r)
{
    (*l) = sparx_ROTL((*l), 9);
    (*l) += (*r);
    (*r) = sparx_ROTL((*r), 2);
    (*r) ^= (*l);
}

void Sparx::L_2(uint16_t * x)
{
    uint16_t tmp = sparx_ROTL((x[0] ^ x[1]), 8);
    x[2] ^= x[0] ^ tmp;
    x[3] ^= x[1] ^ tmp;
    sparx_SWAP(x[0], x[2]);
    sparx_SWAP(x[1], x[3]);
}

void Sparx::L_4(uint16_t * x)
{
    uint16_t tmp = x[0] ^ x[1] ^ x[2] ^ x[3];
    tmp = sparx_ROTL(tmp, 8);

    x[4] ^= x[2] ^ tmp;
    x[5] ^= x[1] ^ tmp;
    x[6] ^= x[0] ^ tmp;
    x[7] ^= x[3] ^ tmp;

    sparx_SWAP(x[0], x[4]);
    sparx_SWAP(x[1], x[5]);
    sparx_SWAP(x[2], x[6]);
    sparx_SWAP(x[3], x[7]);
}

void Sparx::K_perm_64_128(uint16_t * k, uint16_t c)
{
    uint16_t tmp_0, tmp_1, i;
    /* Misty-like transformation */
    A(k+0, k+1);
    k[2] += k[0];
    k[3] += k[1];
    k[7] += c;
    /* Branch rotation */
    tmp_0 = k[6];
    tmp_1 = k[7];
    for (i=7 ; i>=2 ; i--)
    {
        k[i] = k[i-2];
    }
    k[0] = tmp_0;
    k[1] = tmp_1;
}

uint16_t Sparx::merge_8_to_16(uint8_t higher_part, uint8_t lower_part)
{
	uint16_t tmp = (uint16_t)((higher_part << 8) | (lower_part & 0xFF));
	return tmp;
}

void Sparx::K_perm_128_128(uint16_t * k, uint16_t c)
{
    uint16_t tmp_0, tmp_1, i;
    /* Misty-like transformation */
    A(k+0, k+1);
    k[2] += k[0];
    k[3] += k[1];
    A(k+4, k+5);
    k[6] += k[4];
    k[7] += k[5] + c;
    /* Branch rotation */
    tmp_0 = k[6];
    tmp_1 = k[7];
    for (i=7 ; i>=2 ; i--)
    {
        k[i] = k[i-2];
    }
    k[0] = tmp_0;
    k[1] = tmp_1;
}

void Sparx::key_schedule(uint16_t **subkeys, uint16_t master_key[])
{
    uint8_t c, i;
    for (c=0 ; c<(nBranches*NSTEPS+1) ; c++)
    {
        for (i=0 ; i<2*roundPerSteps ; i++)
        {
            subkeys[c][i] = master_key[i];
        }
		if(8 == blockSize)
		{
        	K_perm_64_128(master_key, c+1);
		}
		else
		{
			K_perm_128_128(master_key, c+1);
		}
		
    }
}

void Sparx::Encrypt(uint16_t * x, uint16_t **k)
{
    uint8_t s, r, b;

    s=0; b=0; r=0;
    for (s=0 ; s<NSTEPS ; s++)
    {
        for (b=0 ; b<nBranches ; b++)
        {
            for (r=0 ; r<roundPerSteps ; r++)
            {
                x[2*b  ] ^= k[nBranches*s + b][2*r    ];
                x[2*b+1] ^= k[nBranches*s + b][2*r + 1];
                A(x + 2*b, x + 2*b+1);
            }
        }
		if (8 == blockSize)
		{
			L_2(x);
		}
		else
		{
			L_4(x);
		}
    }
    for (b=0 ; b<nBranches ; b++)
    {
        x[2*b  ] ^= k[nBranches*NSTEPS][2*b  ];
        x[2*b+1] ^= k[nBranches*NSTEPS][2*b+1];
    }
}

void Sparx::query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response)
{
	// allocate master key and subkeys
	uint16_t master_key[2*KSIZE] = {0};
	uint16_t **k = new uint16_t*[nBranches*NSTEPS+1];
	for(int i = 0; i < nBranches*NSTEPS+1; i++)
	{
		k[i] = new uint16_t[2*roundPerSteps];
	}

	// copy master key from PUF
	for(int i = 0; i < SPuf::key.size(); i+=2)
	{
		master_key[i/2] = merge_8_to_16(SPuf::key[i],SPuf::key[i+1]);
	}
	key_schedule(k,master_key);

	if(8 == blockSize)
	{
		switch (blockMode)
		{
		case SPARX_BM_ECB:
			{
				// prepare plaintext
				uint16_t *pt1 = new uint16_t[2*nBranches];
				uint16_t *pt2 = new uint16_t[2*nBranches];

				for(int i = 0; i < challenge->size(); i+=2)
				{
					pt1[i/2] = merge_8_to_16((*challenge)[i], (*challenge)[i+1]);
					pt2[i/2] = merge_8_to_16((*challenge)[i+8], (*challenge)[i+9]);
				}

				// encrypt
				Encrypt(pt1, k);
				Encrypt(pt2, k);

				// convert output
				for (int i = 0; i < 2*nBranches; i++)
				{
					(*response)[2*i] = (uint8_t)((pt1[i] & 0xFF00) >> 8);
					(*response)[2*i+1] = (uint8_t)(pt1[i] & 0xFF);
					(*response)[8+2*i] = (uint8_t)((pt2[i] & 0xFF00) >> 8);
					(*response)[8+2*i+1] = (uint8_t)(pt2[i] & 0xFF);
				}

				delete[] pt1;
				delete[] pt2;
				
				break;
			}

		case SPARX_BM_CBC:
			{
				// prepare plaintext
				uint16_t *pt1 = new uint16_t[2*nBranches];
				uint16_t *pt2 = new uint16_t[2*nBranches];

				//prepare initialization vector
				srand (time(NULL));

				// XOR first half of plaintext with IV
				for(int i = 0; i < challenge->size(); i+=2)
				{
					pt1[i/2] = merge_8_to_16((*challenge)[i], (*challenge)[i+1]) ^ (rand() % 0xFFFF);
				}

				// encrypt first half
				Encrypt(pt1, k);

				// XOR first half of ciphertext with the second half of plaintext
				for(int i = 0; i < challenge->size(); i+=2)
				{
					pt2[i/2] = merge_8_to_16((*challenge)[i+8], (*challenge)[i+9]) ^ pt1[i/2];
				}

				// encrypt second half
				Encrypt(pt2, k);

				// convert output
				for (int i = 0; i < 2*nBranches; i++)
				{
					(*response)[2*i] = (uint8_t)((pt1[i] & 0xFF00) >> 8);
					(*response)[2*i+1] = (uint8_t)(pt1[i] & 0xFF);
					(*response)[8+2*i] = (uint8_t)((pt2[i] & 0xFF00) >> 8);
					(*response)[8+2*i+1] = (uint8_t)(pt2[i] & 0xFF);
				}

				delete[] pt1;
				delete[] pt2;

				break;
			}
		
		default:
			std::cerr << "\nNo Sparx block mode selected\n";
			break;
		}
		
	}
	else if(16 == blockSize)
	{
		uint16_t master_key[2*KSIZE] = {0};
		
		// allocate plaintext
		uint16_t *pt = new uint16_t[2*nBranches];
		for(int i = 0; i < challenge->size(); i+=2)
		{
			pt[i/2] = merge_8_to_16((*challenge)[i], (*challenge)[i+1]);
		}

		// init master_key
		for(int i = 0; i < SPuf::key.size(); i+=2)
		{
			master_key[i/2] = merge_8_to_16(SPuf::key[i],SPuf::key[i+1]);
		}

		// allocate sub keys
		uint16_t **k = new uint16_t*[nBranches*NSTEPS+1];
		for(int i = 0; i < nBranches*NSTEPS+1; i++)
		{
			k[i] = new uint16_t[2*roundPerSteps];
		}

		// key is always 128 bits
		key_schedule(k,master_key);

		// encypt
		Encrypt(pt, k);

		// convert output
		for (int i = 0; i < 2*nBranches; i++)
		{
			(*response)[2*i] = (uint8_t)((pt[i] & 0xFF00) >> 8);
			(*response)[2*i+1] = (uint8_t)(pt[i] & 0xFF);
		}

		delete[] pt;
	}

	// deallocate subkeys
	for(int i = 0; i < nBranches*NSTEPS+1; i++)
	{
		delete[] k[i];
	}
	delete[] k;
}