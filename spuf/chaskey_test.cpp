#include "chaskey.h"

int main()
{
	std::array<uint8_t,16> v = 
	{
		0xb8, 0x23, 0x28, 0x26,
		0xfd, 0x5e, 0x40, 0x5e,
		0x69, 0xa3, 0x01, 0xa9,
		0x78, 0xea, 0x7a, 0xd8
	};
	
	std::array<uint8_t,16> key = 
	{
		0x56, 0x09, 0xe9, 0x68,
    	0x5f, 0x58, 0xe3, 0x29,
    	0x40, 0xec, 0xec, 0x98,
    	0xc5, 0x22, 0x98, 0x2f
	};

	std::array<uint8_t,16> out = {0};

	uint32_t *v32 = (uint32_t*) &v[0];
	uint32_t *key32 = (uint32_t*) &key[0];

	SPuf *cipher = new Chaskey(CHASKEY_LTS,key);

	std::cout << "PLAINTEXT: \t";
	cipher->print_bitstring(v);
	std::cout << std::endl;

	std::cout << "KEY: \t\t";
	cipher->print_bitstring(key);
	std::cout << std::endl;

	cipher->query(&v,&out);

	std::cout << "CIPHERTEXT: \t";
	cipher->print_bitstring(out);
	std::cout << std::endl;

	delete cipher;

    return 0;
}