#include "zorro.h"

int main()
{
	std::array<uint8_t,16> key = {
		0x74, 0xdd, 0x31, 0xf8, 0x0d, 0x81, 0x41, 0x1a, 
		0xec, 0x50, 0xc9, 0xe0, 0xc6, 0x81, 0x3a, 0xc2
	};

	std::array<uint8_t,16> plaintext = {
		0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x16, 0x30
	};

	const std::array<uint8_t,16> expected_ct = {
		0xAF, 0x1E, 0x0A, 0x2E, 0x12, 0x61, 0xAC, 0x20, 
		0x7A, 0x0C, 0x58, 0x11, 0x7A, 0x3E, 0xBD, 0xA4
	};

	std::array<uint8_t,16> ciphertext = {0};

	SPuf *cipher = new Zorro(key);
	cipher->query(&plaintext,&ciphertext);

	std::cout << "Plaintext: \t\t";
	cipher->print_bitstring(plaintext);

	std::cout << "Key: \t\t\t";
	cipher->print_bitstring(key);

	std::cout << "Expected Ciphertext: \t";
	cipher->print_bitstring(expected_ct);

	std::cout << "Ciphertext: \t\t";
	cipher->print_bitstring(ciphertext);

	if(0 == memcmp(&expected_ct,&ciphertext,16))
	{
		std::cout << "\nTEST SUCCESSFUL!\n";
	}

	delete cipher;

	return 0;
}