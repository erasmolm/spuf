#include "present.h"

int main()
{
	std::array<uint8_t,16> key = {0};

	std::array<uint8_t,16> plaintext = {0};

	const std::array<uint8_t,16> expected_ct = 
	{
		0xAF, 0x00, 0x69, 0x2E,
		0x2A, 0x70, 0xDB, 0x96
	};

	const std::array<uint8_t,16> expected_ct_ecb = 
	{
		0xAF, 0x00, 0x69, 0x2E,
		0x2A, 0x70, 0xDB, 0x96,
		0xAF, 0x00, 0x69, 0x2E,
		0x2A, 0x70, 0xDB, 0x96
	};

	std::array<uint8_t,16> expected_ct_cbc = 
	{
		0xAF, 0x00, 0x69, 0x2E,
		0x2A, 0x70, 0xDB, 0x96,
		0,0,0,0,0,0,0,0 // will be set later
	};

	std::array<uint8_t,16> ciphertext = {0};

	// DEFAULT MODE

	SPuf *cipher = new Present(PRESENT_BM_NONE,key);
	cipher->query(&plaintext,&ciphertext);

	std::cout << "\nTEST DEFAULT MODE: \n";

	std::cout << "Plaintext \t\t(8  bytes): ";
	cipher->print_bitstring(plaintext);

	std::cout << "Key \t\t\t(16 bytes): ";
	cipher->print_bitstring(key);

	std::cout << "Expected Ciphertext \t(8  bytes): ";
	cipher->print_bitstring(expected_ct);

	std::cout << "Ciphertext \t\t(8  bytes): ";
	cipher->print_bitstring(ciphertext);


	if(0 == memcmp(&expected_ct,&ciphertext,8))
	{
		std::cout << "\nTEST DEFAULT MODE SUCCESSFUL!\n";
	}

	// ECB MODE

	SPuf *cipher_ECB = new Present(PRESENT_BM_ECB,key);
	cipher_ECB->query(&plaintext,&ciphertext);

	std::cout << "\nTEST ECB MODE: \n";

	std::cout << "Plaintext \t\t(16 bytes): ";
	cipher_ECB->print_bitstring(plaintext);

	std::cout << "Key \t\t\t(16 bytes): ";
	cipher_ECB->print_bitstring(key);

	std::cout << "Expected Ciphertext \t(16 bytes): ";
	cipher_ECB->print_bitstring(expected_ct_ecb);

	std::cout << "Ciphertext \t\t(16 bytes): ";
	cipher_ECB->print_bitstring(ciphertext);

	if(0 == memcmp(&expected_ct_ecb,&ciphertext,16))
	{
		std::cout << "\nTEST ECB MODE SUCCESSFUL!\n";
	}

	// CBC MODE (null IV)

	cipher->query(&expected_ct_cbc, &ciphertext);
	
	for(int i = 0; i < 8; i++)
	{
		expected_ct_cbc[8 + i] = ciphertext[i];
	}

	for (int i = 0; i < 16; i++)
	{
		ciphertext[i] = 0;
	}

	SPuf *cipher_CBC = new Present(PRESENT_BM_CBC_TEST,key);
	cipher_CBC->query(&plaintext,&ciphertext);

	std::cout << "\nTEST CBC MODE: \n";

	std::cout << "Plaintext \t\t(16 bytes): ";
	cipher_CBC->print_bitstring(plaintext);

	std::cout << "Key \t\t\t(16 bytes): ";
	cipher_CBC->print_bitstring(key);

	std::cout << "Expected Ciphertext \t(16 bytes): ";
	cipher_CBC->print_bitstring(expected_ct_cbc);

	std::cout << "Ciphertext \t\t(16 bytes): ";
	cipher_CBC->print_bitstring(ciphertext);

	if(0 == memcmp(&expected_ct_cbc,&ciphertext,16))
	{
		std::cout << "\nTEST CBC MODE SUCCESSFUL!\n";
	}

	delete cipher;
	delete cipher_ECB;
	delete cipher_CBC;

	return 0;
}