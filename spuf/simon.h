//
// Created by Mario Barbareschi on 21/05/2020.
//

#ifndef SPUF_SIMON_H
#define SPUF_SIMON_H

#include "spuf.h"

enum simon_mode_t
{
	ECB,
	CTR,
	CBC,
	CFB,
	OFB
};

static const uint8_t block_sizes[] = {32, 48, 48, 64, 64, 96, 96, 128, 128, 128};

static const uint16_t key_sizes[] = {64, 72, 96, 96, 128, 96, 144, 128, 192, 256};

enum cipher_config_t
{
	cfg_64_32,
	cfg_72_48,
	cfg_96_48,
	cfg_96_64,
	cfg_128_64,
	cfg_96_96,
	cfg_144_96,
	cfg_128_128,
	cfg_192_128,
	cfg_256_128
};

typedef struct
{
	enum cipher_config_t cipher_cfg;
	void (*encryptPtr)(const uint8_t, const uint8_t *, const uint8_t *, uint8_t *);
	void (*decryptPtr)(const uint8_t, const uint8_t *, const uint8_t *, uint8_t *);
	uint16_t key_size;
	uint8_t block_size;
	uint8_t round_limit;
	uint8_t init_vector[16];
	uint8_t counter[16];
	uint8_t key_schedule[576];
	uint8_t alpha;
	uint8_t beta;
	uint8_t z_seq;
} SimSpk_Cipher;

typedef struct _bword_24
{
	uint32_t data : 24;
} bword_24;

typedef struct _bword_48
{
	uint64_t data : 48;
} bword_48;

// Cipher Operation Macros
#define shift_one(x_word) (((x_word) << 1) | ((x_word) >> (word_size - 1)))
#define shift_eight(x_word) (((x_word) << 8) | ((x_word) >> (word_size - 8)))
#define shift_two(x_word) (((x_word) << 2) | ((x_word) >> (word_size - 2)))

#define rshift_three(x) (((x) >> 3) | (((x)&0x7) << (word_size - 3)))
#define rshift_one(x) (((x) >> 1) | (((x)&0x1) << (word_size - 1)))

void Simon_Encrypt_32(uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					  uint8_t *ciphertext);
void Simon_Encrypt_48(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					  uint8_t *ciphertext);
void Simon_Encrypt_64(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					  uint8_t *ciphertext);
void Simon_Encrypt_96(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					  uint8_t *ciphertext);
void Simon_Encrypt_128(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *plaintext,
					   uint8_t *ciphertext);

void Simon_Decrypt_32(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext);
void Simon_Decrypt_48(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext);
void Simon_Decrypt_64(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext);
void Simon_Decrypt_96(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					  uint8_t *plaintext);
void Simon_Decrypt_128(const uint8_t round_limit, const uint8_t *key_schedule, const uint8_t *ciphertext,
					   uint8_t *plaintext);

class Simon : public SPuf::SPuf
{
private:
	uint64_t z_arrays[5] = {0b0001100111000011010100100010111110110011100001101010010001011111,
							0b0001011010000110010011111011100010101101000011001001111101110001,
							0b0011001101101001111110001000010100011001001011000000111011110101,
							0b0011110000101100111001010001001000000111101001100011010111011011,
							0b0011110111001001010011000011101000000100011011010110011110001011};

	// Valid Cipher Parameters
	const uint8_t simon_rounds[10] = {32, 36, 36, 42, 44, 52, 54, 68, 69, 72};
	const uint8_t z_assign[10] = {0, 0, 1, 2, 3, 2, 3, 2, 3, 4};

	uint8_t my_IV[4] = {0x32, 0x14, 0x76, 0x58};
	uint8_t my_counter[4] = {0x2F, 0x3D, 0x5C, 0x7B};

	SimSpk_Cipher my_simon_cipher;

	uint8_t Simon_Init(SimSpk_Cipher *cipher_object, enum cipher_config_t cipher_cfg, enum simon_mode_t c_mode, uint8_t *key, uint8_t *iv, uint8_t *counter)
	{

		if (cipher_cfg > cfg_256_128 || cipher_cfg < cfg_64_32)
		{
			return 1;
		}

		cipher_object->block_size = block_sizes[cipher_cfg];
		cipher_object->key_size = key_sizes[cipher_cfg];
		cipher_object->round_limit = simon_rounds[cipher_cfg];
		cipher_object->cipher_cfg = cipher_cfg;
		cipher_object->z_seq = z_assign[cipher_cfg];
		uint8_t word_size = block_sizes[cipher_cfg] >> 1;
		uint8_t word_bytes = word_size >> 3;
		uint16_t key_words = key_sizes[cipher_cfg] / word_size;
		uint64_t sub_keys[4] = {};
		uint64_t mod_mask = ULLONG_MAX >> (64 - word_size);

		// Setup
		for (int i = 0; i < key_words; i++)
		{
			memcpy(&sub_keys[i], key + (word_bytes * i), word_bytes);
		}

		uint64_t tmp1, tmp2;
		uint64_t c = 0xFFFFFFFFFFFFFFFC;

		// Store First Key Schedule Entry
		memcpy(cipher_object->key_schedule, &sub_keys[0], word_bytes);

		for (int i = 0; i < simon_rounds[cipher_cfg] - 1; i++)
		{
			tmp1 = rshift_three(sub_keys[key_words - 1]);

			if (key_words == 4)
			{
				tmp1 ^= sub_keys[1];
			}

			tmp2 = rshift_one(tmp1);
			tmp1 ^= sub_keys[0];
			tmp1 ^= tmp2;

			tmp2 = c ^ ((z_arrays[cipher_object->z_seq] >> (i % 62)) & 1);

			tmp1 ^= tmp2;

			// Shift Sub Words
			for (int j = 0; j < (key_words - 1); j++)
			{
				sub_keys[j] = sub_keys[j + 1];
			}
			sub_keys[key_words - 1] = tmp1 & mod_mask;

			// Append sub key to key schedule
			memcpy(cipher_object->key_schedule + (word_bytes * (i + 1)), &sub_keys[0], word_bytes);
		}

		if (cipher_cfg == cfg_64_32)
		{
			cipher_object->encryptPtr = &Simon_Encrypt_32;
			cipher_object->decryptPtr = &Simon_Decrypt_32;
		}
		else if (cipher_cfg <= cfg_96_48)
		{
			cipher_object->encryptPtr = Simon_Encrypt_48;
			cipher_object->decryptPtr = Simon_Decrypt_48;
		}
		else if (cipher_cfg <= cfg_128_64)
		{
			cipher_object->encryptPtr = Simon_Encrypt_64;
			cipher_object->decryptPtr = Simon_Decrypt_64;
		}

		else if (cipher_cfg <= cfg_144_96)
		{
			cipher_object->encryptPtr = Simon_Encrypt_96;
			cipher_object->decryptPtr = Simon_Decrypt_96;
		}

		else if (cipher_cfg <= cfg_256_128)
		{
			cipher_object->encryptPtr = Simon_Encrypt_128;
			cipher_object->decryptPtr = Simon_Decrypt_128;
		}

		else
			return 1;

		return 0;
	}

	uint8_t Simon_Encrypt(SimSpk_Cipher cipher_object, const uint8_t *plaintext, uint8_t *ciphertext)
	{
		(*cipher_object.encryptPtr)(cipher_object.round_limit, cipher_object.key_schedule, plaintext, ciphertext);
		return 0;
	}

	uint8_t Simon_Decrypt(SimSpk_Cipher cipher_object, const uint8_t *ciphertext, uint8_t *plaintext)
	{
		(*cipher_object.decryptPtr)(cipher_object.round_limit, cipher_object.key_schedule, ciphertext, plaintext);
		return 0;
	}

public:
	Simon() : SPuf()
	{
		Simon_Init(&my_simon_cipher, cfg_128_128, ECB, SPuf::key.data(), my_IV, my_counter);
	}

	Simon(const std::array<uint8_t, 16> config_key) : SPuf(config_key)
	{
		Simon_Init(&my_simon_cipher, cfg_128_128, ECB, SPuf::key.data(), my_IV, my_counter);
	}
	void setKey(std::array<uint8_t, 16> config_key);

	void query(const std::array<uint8_t, 16> *const challenge, std::array<uint8_t, 16> *const response);
};

#endif //SPUF_SIMON_H
