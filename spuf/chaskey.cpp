#include "chaskey.h"

Chaskey::Chaskey() : SPuf()
{
	// 8 rounds as default case
	Chaskey::rounds = chaskey_rounds[CHASKEY_8];
}

Chaskey::Chaskey(chaskey_mode_t mode) : SPuf()
{
	Chaskey::rounds = chaskey_rounds[mode];
}

Chaskey::Chaskey(chaskey_mode_t mode, const std::array<uint8_t, 16> key) : SPuf(key)
{
	Chaskey::rounds = chaskey_rounds[mode];
}

void Chaskey::setKey(std::array<uint8_t, 16> config_key)
{
	SPuf::key = config_key;
}


void Chaskey::encrypt(uint32_t v[4], uint32_t key[4])
{
	for (int i = 0; i < 4; i++)
	{
		v[i] ^= key[i];
	}

	for (int i = 0; i < rounds; i++)
	{
		v[0] += v[1];
		v[1] = ROTL(v[1], 5);
		v[1] ^= v[0];
		v[0] = ROTL(v[0], 16);
		v[2] += v[3];
		v[3] = ROTL(v[3], 8);
		v[3] ^= v[2];
		v[0] += v[3];
		v[3] = ROTL(v[3], 13);
		v[3] ^= v[0];
		v[2] += v[1];
		v[1] = ROTL(v[1], 7);
		v[1] ^= v[2];
		v[2] = ROTL(v[2], 16);
	}

	for (int i = 0; i < 4; i++)
	{
		v[i] ^= key[i];
	}
}

void Chaskey::query(const std::array<uint8_t, 16>* const challenge, std::array<uint8_t, 16>* const response)
{
	uint32_t *v32 = (uint32_t*) &((*challenge)[0]);
	uint32_t *key32 = (uint32_t*) &(SPuf::key[0]);

	encrypt(v32, key32);

	for(int i = 0; i < response->size(); i++)
	{
		(*response)[i] = (*challenge)[i];
	}
}