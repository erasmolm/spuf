from common import int_to_binstr
from common import hexstr_to_bitarray
from multiprocessing import Process as pr
from multiprocessing import Pool

import glob
import time
import numpy as np

def calculateCipherUniformity(fname):
	ciphername = fname.replace(pathname,"").replace(".txt","")
	f = open(fname, "r")

	# ogni l è una stringa esadecimale
	r_len = len(f.readlines())
	a = np.zeros((r_len,128), dtype=np.uint8)
	f.seek(0)

	i = 0
	for l in f:
		hexstr_to_bitarray(l.strip('\n'),a[i])
		i += 1

	f.close()

	# Single response uniformity
	s_unif = np.zeros(r_len)
	for i in range(0,r_len):
		s_unif[i] = np.mean(a[i])
	# dictlog[ciphername] = s_unif
	results = {ciphername : s_unif}

	# Global uniformity
	g_unif = np.mean(a)
	print("Found cipher " + ciphername + "\tfile: " + fname + "\tG_Unif = " + str(g_unif))

	return results

##########################################################

start_time = time.time()

pathname = "1M_extracted/responses_"
files = glob.glob(pathname + "*.txt")

with Pool() as pool:
	txtlog = pool.map(calculateCipherUniformity, files)

dictlog = dict()
for i in txtlog:
	# print(i)
	dictlog.update(i)

np.save('data_unif.npy',dictlog)

print("--- %s seconds ---" % (time.time() - start_time))