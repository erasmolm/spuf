##########################################################
# GRAPH
##########################################################

from common import sortDict
import matplotlib.pyplot as ppl
import numpy as np
import json

data = np.load('output_data/1M_data_bias.npy',allow_pickle=True)
dictlog = data.item()

# ESCLUDE XORPUF
dictlog.pop('xorpuf')

# Bias Distributions
# NB: scartate perchè poco significative
# for k in dictlog:
# 	ppl.figure()
# 	ppl.title(k + " Bias Distribution")
# 	ppl.hist(dictlog[k])

# Bias MSE

msed = dict()
for k in dictlog:
	msed[k] = np.mean(np.square(dictlog[k] - 0.5)) # MSE

msed = sortDict(msed)

labels = list(msed.keys())
sizes = list(msed.values())
index = [i for i in range(1,len(sizes)+1)]

print(labels)
print(sizes)

ppl.figure("Bias MSE")
ppl.barh(index, sizes, tick_label=labels)

ppl.show()
ppl.savefig('figures/bias_mse.pdf')