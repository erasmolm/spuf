from common import hexstr_to_binstr as hbstr
from common import int_to_binstr as ibstr
from multiprocessing import Process as pr
from multiprocessing import Pool

import json
import time
import glob
import numpy as np

def bias(responses,n):
	return np.mean(responses[:,n])

def calculateCipherBias(fname):
	ciphername = fname.replace(pathname,"").replace(".txt","")
	f = open(fname, "r")

	r_len = len(f.readlines())					# num of responses
	a = np.zeros((r_len,128), dtype=np.uint8)	# bit matrix
	f.seek(0)

	# convert responses strings to bit matrix
	i = 0
	for l in f:							# for each response
		j = 0
		binstr = hbstr(l.rstrip('\n'))
		for bit in binstr:				# for each bit of response
			a[i,j] = int(bit, 2)
			j += 1
		i += 1

	f.close()

	v_a = np.zeros(128)
	for i in range(0,128):	# i: 0..127
		v_a[i] = bias(a,i)
	
	mse = np.mean(np.square(v_a - 0.5))
	print("Found cipher " + ciphername + "\tfile: " + fname + "\tMSE Bias = " + str(mse))
	# dictlog[ciphername] = v_a
	results = {ciphername : v_a}

	return results

##########################################################

start_time = time.time()

pathname = "1M_extracted/responses_"
files = glob.glob(pathname + "*.txt")

with Pool() as pool:
	txtlog = pool.map(calculateCipherBias, files)

dictlog = dict()
for i in txtlog:
	# print(i)
	dictlog.update(i)

np.save('data_bias.npy',dictlog)

print("--- %s seconds ---" % (time.time() - start_time))