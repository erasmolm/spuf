def int_to_binstr(n, fill=0):
	# assert isinstance(n, int)
	return bin(n)[2:].zfill(fill)

def hexstr_to_binstr(bs):
	# assert isinstance(bs, str)
	n = 4*len(bs)
	return int_to_binstr(int(bs,16), n)

def hexstr_to_bitarray(hs,barr):
	import numpy as np
	# assert isinstance(hs, str)
	# assert hs.isupper()
	n = 4*len(hs)
	assert isinstance(barr,np.ndarray)
	assert n == barr.size
	lut = {
		"0" : np.array([0,0,0,0], dtype=np.uint8),
		"1" : np.array([0,0,0,1], dtype=np.uint8),
		"2" : np.array([0,0,1,0], dtype=np.uint8),
		"3" : np.array([0,0,1,1], dtype=np.uint8),
		"4" : np.array([0,1,0,0], dtype=np.uint8),
		"5" : np.array([0,1,0,1], dtype=np.uint8),
		"6" : np.array([0,1,1,0], dtype=np.uint8),
		"7" : np.array([0,1,1,1], dtype=np.uint8),
		"8" : np.array([1,0,0,0], dtype=np.uint8),
		"9" : np.array([1,0,0,1], dtype=np.uint8),
		"A" : np.array([1,0,1,0], dtype=np.uint8),
		"B" : np.array([1,0,1,1], dtype=np.uint8),
		"C" : np.array([1,1,0,0], dtype=np.uint8),
		"D" : np.array([1,1,0,1], dtype=np.uint8),
		"E" : np.array([1,1,1,0], dtype=np.uint8),
		"F" : np.array([1,1,1,1], dtype=np.uint8),
	}
	i = 0
	for c in hs:
		barr[i:i+4] = lut[c]
		i += 4

def sortDict(d):
	return {k: v for k, v in sorted(d.items(), key=lambda item: item[1],reverse=True)}
