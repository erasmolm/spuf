from common import int_to_binstr as ibstr
from common import hexstr_to_binstr as hbstr
from multiprocessing import Process as pr
from multiprocessing import Pool

import json
import numpy as np
import time
import glob


def calculateCipherUniqueness(fname):
	ciphername = fname.replace(pathname,"").replace(".txt","")
	f = open(fname, "r")

	responses = []

	for l in f:
		responses.append(l.rstrip('\n'))

	f.close()
	r = len(responses)
	g_uniq = 0

	for i in range(r-1): 				# i: 0..R-2
		for j in range(i+1,r): 			# j: i+1..R
			ri = int(responses[i], 16)
			rj = int(responses[j], 16)
			g_uniq = g_uniq + bin(ri^rj).count('1')/128	# FHD

	g_uniq = 2*g_uniq / (r*(r-1))

	# results = str("Found cipher " + ciphername + "\t\tfile: " + fname + "\t\tUniqueness = " + str(g_uniq))
	results = {ciphername : g_uniq}

	return results

##########################################################

start_time = time.time()

pathname = "extracted/responses_"
files = glob.glob(pathname + "*.txt")

with Pool() as pool:
	txtlog = pool.map(calculateCipherUniqueness, files)

dictlog = dict()
for i in txtlog:
	print(i)
	dictlog.update(i)

with open('data_uniq.json', 'w') as fp:
    json.dump(dictlog, fp)

print("--- %s seconds ---" % (time.time() - start_time))