##########################################################
# GRAPH
##########################################################

import matplotlib.pyplot as ppl
import numpy as np
import json

from common import sortDict

with open('output_data/100k_data_uniq.json', 'r') as fp:
    dictlog = json.load(fp)

# ESCLUDE XORPUF
dictlog.pop('xorpuf')

# Uniqueness

for k in dictlog:
	dictlog[k] = np.mean(np.square(dictlog[k] - 0.5)) # MSE

dictlog = sortDict(dictlog)

labels = list(dictlog.keys())
sizes = list(dictlog.values())

print(labels)
print(sizes)

index = [i for i in range(1,len(sizes)+1)]

ppl.figure("Uniqueness MSE")
ppl.barh(index, sizes, tick_label=labels)

ppl.show()
ppl.savefig('figures/uinq_mse.pdf')