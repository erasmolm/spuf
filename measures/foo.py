import gmpy2 as gm
import time

lut = {
	"0" : 0,
	"1" : 1,
	"2" : 1,
	"3" : 2,
	"4" : 1,
	"5" : 2,
	"6" : 2,
	"7" : 3,
	"8" : 1,
	"9" : 2,
	"A" : 2,
	"B" : 3,
	"C" : 2,
	"D" : 3,
	"E" : 3,
	"F" : 4,
}

s = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'
n = int(s,16)
print('count')

t = time.time()
p = bin(n).count('1')
print(time.time() - t)
print(p)


print('gmpy')

t = time.time()
p = gm.popcount(n)
print(time.time() - t)
print(p)

print('lut')

t = time.time()

p = 0
for nib in s:
	p += lut[nib]

print(time.time() - t)
print(p)