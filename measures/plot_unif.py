##########################################################
# GRAPH
##########################################################

import matplotlib.pyplot as ppl
import scipy.stats as stats
import numpy as np

from common import sortDict

data = np.load('output_data/1M_data_unif.npy', allow_pickle=True)
dictlog = data.item()

# Uniformity Distributions
ppl.rc('xtick',labelsize=13)
ppl.rc('ytick',labelsize=13)

for k in dictlog:
    ppl.figure(k + " Uniformity Distribution")
    val = dictlog[k]
    x = np.linspace(0, 1, 1000)
    dev = np.std(val)
    y = stats.norm.pdf(x,0.5, dev)
    sigma = np.std(val)
    n, bins, _ = ppl.hist(
        val,
        histtype='stepfilled',
        alpha=0.7,
        # weights=np.ones(len(val))/len(val),
        # bins=100,
        density=True,
        stacked=False,
        )
    ppl.plot(x, y,'r')
    # print("Area below the integral: ", np.sum(n * np.diff(bins)))
    ppl.savefig('figures/'+ k + '_distribution.pdf')



# Global Uniformity MSE

# ESCLUDE XORPUF
dictlog.pop('xorpuf')

ud = dict()
for k in dictlog:
	g_unif = np.mean(dictlog[k])			# mean of all response uniformities
	# ud[k] = np.abs(0.5 - g_unif)
	ud[k] = np.mean(np.square(g_unif - 0.5)) # MSE

ud = sortDict(ud)

labels = list(ud.keys())
sizes = list(ud.values())
print(labels)
print(sizes)
index = [i for i in range(1,len(sizes)+1)]

ppl.figure("Global Uniformity MSE")
ppl.barh(index, sizes, tick_label=labels)

ppl.show()
ppl.savefig('figures/g_unif_mse.pdf')
