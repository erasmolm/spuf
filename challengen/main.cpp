#include <iostream>
#include <array>
#include <random>
#include <time.h>
#include <fstream>
#include <iomanip>

const std::string challengepath = "keys.txt";

int main() {
    std::cout << "..:: Challengen ::.." << std::endl;

    std::ofstream challengesfile;
    challengesfile.open(challengepath, std::ifstream::out);
    if(!challengesfile.is_open()){
        std::cerr<< "File "<< challengepath << " not found" << std::endl;
        std::exit(-1);
    }

    const int n = 16;

    srand(time(NULL));
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0.0,1.0);
    std::string challenge_string;

    challenge_string = "";
    std::array<uint8_t, 16> rnd_challenge;

    for(int k = 0; k < 300000; ++k){
        for (int i = 0; i < n; ++i) {
            rnd_challenge[i] = (uint8_t) 255*distribution(generator);
            challengesfile<< std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)rnd_challenge[i];
        }
        challengesfile << std::endl;
        std::flush(challengesfile);
    }

    return 0;
}
